# coding =utf-8
import re

import os, json, re

from nltk import word_tokenize
from joblib import Parallel, delayed

from .models.spatial_relation import get_spatial_relations
from .helpers.relation_cache import relation_cache
from .models.str import STR
from .models.transformation.transform import Generalisation, Expansion


from .nlp.disambiguator import *
from .nlp.ner import Spacy, NER

from .nlp.exception.disambiguator import NotADisambiguatorInstance
from .nlp.exception.ner import NotANERInstance
from multiprocessing import cpu_count as cpu_count_system

from mytoolbox.env import in_notebook
if in_notebook():
    from tqdm._tqdm_notebook import tqdm_notebook as tqdm
else:
    from tqdm import tqdm


class Pipeline(object):
    """
    Class defining a Pipeline instance
    Run the whole treatement on a given text
    """

    def __init__(self,lang="en",**kwargs):
        """
        Constructor


        :param kwargs:
        """
        self.lang=lang[:2]
        self.ner = kwargs["ner"] if "ner" in kwargs else Spacy(lang=lang[:2])
        self.disambiguator=kwargs["disambiguator"] if "disambiguator" in kwargs else WikipediaDisambiguator()

        self.corpus_name = kwargs["corpus_name"] if "corpus_name" in kwargs else "no_name"
        self.no_name = False
        if self.corpus_name == "no_name":
            self.no_name = True
        self.corpus_name= "{0}_{1}".format(self.corpus_name,self.lang)


        self.verbose = kwargs.get("verbose",False)

    def parse(self,text,debug=False,stop_words=[]):
        """

        :param text:
        :rtype: list,dict
        """
        output = text

        # NER
        output = self.ner.identify(output)
        # Disambiguation
        se_identified = self.disambiguator.disambiguate(self.lang,ner_output=output)
        for top_, id in list(se_identified.items()):
            if not id.startswith("GD") or top_.strip().lower() in stop_words\
                    or re.match("\d+",top_.strip()) or len(re.sub("\d+","",top_))<3:
                del se_identified[top_]
        if debug:
            print(se_identified)

        return text, se_identified


    def set_ner(self,ner):
        """
        Set NER used in the pipeline
        :param ner:
        :return:
        """
        if isinstance(ner,NER):
            self.ner=ner
        else:
            raise NotANERInstance()

    def set_disambiguator(self,disambiguator):
        """

        :param disambiguator:
        :return:
        """
        if isinstance(disambiguator,Disambiguator):
            self.disambiguator=disambiguator
        else:
            raise NotADisambiguatorInstance()

    def extract_all_relation(self,spatial_entities):
        """
        Extract relation information between spatial entities
        Parameters
        ----------
        spatial_entities

        Returns
        -------

        """
        if not self.no_name:
            if os.path.exists("{0}_adj_dict.json".format(self.corpus_name)): #and yes_or_no(question="Do you want to use previous adj file"):
                dict_adj=json.load(open("{0}_adj_dict.json".format(self.corpus_name)))
            if os.path.exists("{0}_inc_dict.json".format(self.corpus_name)):# and yes_or_no(question="Do you want to use previous inc file"):
                dict_inc=json.load(open("{0}_inc_dict.json".format(self.corpus_name)))
            relation_cache.update(dict_adj,dict_inc)
            return

        else:
            data = get_spatial_relations(spatial_entities)
            relation_cache.update(data.adjacency,data.inclusion)
            # Saving
            if not self.no_name:
                open("{0}_adj_dict.json".format(self.corpus_name),'w').write(json.dumps(data.adjacency))
                open("{0}_inc_dict.json".format(self.corpus_name),'w').write(json.dumps(data.inclusion))

    def pipe_build(self,texts, **kwargs):
        # Extract Spatial entities
        cpu_count = kwargs.get("cpu_count",cpu_count_system())
        stop_words = kwargs.get("stop_words",[])
        stop_words = [s.lower() for s in stop_words]

        text_and_spatial_entities = [self.parse(text,stop_words=stop_words) for text in tqdm(texts,desc="Extract spatial entities from the texts", disable=(not self.verbose))]

        # Filter Output
        sp_es= []
        for res in text_and_spatial_entities:
            sp_es.extend(list(res[1].values()))
        sp_es= [es for es in sp_es if es.startswith("GD")]

        if self.verbose :
            print("Extract Spatial Relation for all identified spatial entities")

        self.extract_all_relation(sp_es)

        str_s = Parallel(n_jobs=cpu_count,backend="threading")(delayed(self.build)(ext[1]) for ext in tqdm(text_and_spatial_entities, desc="Build STR", disable=(not self.verbose)))
        return str_s

    def build(self, spatial_entities_identified,extract_rel=False):
        if extract_rel:
            self.extract_all_relation([es_id for es_id in spatial_entities_identified])
        str_ = STR(spatial_entities_identified, toponym_first=True)
        str_.build()
        return str_

    def pipe_transform(self,strs_,**kwargs):
        str_s = [ self.transform(str_, **kwargs) for str_ in tqdm(strs_,desc="Transform STR", disable=(not self.verbose))]
        return str_s

    def transform(self,str_,**kwargs):
        if not "type_trans" in kwargs:
            return str_
        type_trans=kwargs.pop("type_trans")
        if type_trans == "gen":
            str_=Generalisation().transform(str_,**kwargs)
        else:
            str_=Expansion().transform(str_,**kwargs)
        return str_
