# coding = utf-8

import json
import os
from pathlib import Path


class Configuration(object):
    """
    Define the `Configuration` instantiation. The `Configuration` object contains all the
    shared variable of strpython like : the georeferential server address, the Stanford NER address, etc.
    """
    def __init__(self, data):
        """
        Constructor :param data: dict that contains all the configuration variable. In the module, these variables
        are stored in a file at `~/.strpython/configuration.json`.
        """
        self.__dict__ = data
        for d in self.__dict__:
            if isinstance(self.__dict__[d], dict):
                self.__dict__[d] = Configuration(self.__dict__[d])

    def __getitem__(self, item):
        return self.__dict__[item]


"""
Initialise the config variable
Access this variable using `from strpython.config.configuration import config`
"""
home = str(Path.home())
config = Configuration(json.load(open(os.path.join(home, ".strpython/config.json"))))
