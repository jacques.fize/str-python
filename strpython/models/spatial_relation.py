# coding = utf-8
from multiprocessing import cpu_count

from shapely.geometry import Point, Polygon
import geopandas as gpd
import pandas as pd

from mytoolbox.env import in_notebook

if in_notebook():
    from tqdm._tqdm_notebook import tqdm_notebook as tqdm
else:
    from tqdm import tqdm

from strpython.helpers.collision import getGEO
from strpython.helpers.geodict_helpers import gazetteer

from mytoolbox.structure.objectify import objectify


class MetaCollector():
    __cache_entity_data = {}

    def __init__(self,verbose = False):
        self.verbose = verbose

    def get_data(self, id_se):
        """
        Return an gazpy.Element object containing information about a spatial entity.

        Parameters
        ----------
        id_se : str
            Identifier of the spatial entity

        Returns
        -------
        gazpy.Element
            data
        """

        if id_se in MetaCollector.__cache_entity_data:
            return MetaCollector.__cache_entity_data[id_se]
        data = gazetteer.get_by_id(id_se)
        if len(data) > 0:
            MetaCollector.__cache_entity_data[id_se] = data[0]
            return data[0]
        data = {}

    def is_relation(self, id_se1: str, id_se2: str):
        """
        Return True if the relation defined exist between the two entities
        Parameters
        ----------
        id_se1 : str
            Identifier of the first spatial entity
        id_se2 : str
            Identifier of the second spatial entity

        Returns
        -------
        bool
            if a relation exists
        """
        raise NotImplementedError()


class RelationExtractor(MetaCollector):
    __cache_entity_data = {}

    def __init__(self, spatial_entities,verbose = False):
        """
        Constructor
        Parameters
        ----------
        spatial_entities : list
            list of spatial entities identifier
        """
        MetaCollector.__init__(self,verbose)
        self.spatial_entities = list(set(spatial_entities))

        # Retrieve Geometries
        data = [[sp_id, getGEO(sp_id)] for sp_id in
                tqdm(self.spatial_entities, desc="Retrieving Geometries...", disable=(not self.verbose))]

        self.all_geometry = []
        for i in data:
            if not isinstance(i[1], gpd.GeoDataFrame) and not isinstance(i[1], gpd.GeoSeries):
                self.all_geometry.append([i[0], Polygon()])
            else:
                self.all_geometry.append([i[0], i[1].geometry.values[0]])

        self.adjacency_geom, self.inclusion_geom = pd.DataFrame(), pd.DataFrame()
        self.adjacency_meta, self.inclusion_meta = pd.DataFrame(), pd.DataFrame()

    def get_relation_geometry_based(self):
        """
        Extract adjacency and inclusion relation based on the geometries of each spatial entities.

        Each relation is extracted based on the following logical operation:
         * Adj(Sa, Sb) = (Intersect(Sa,Sb) XOR (Within(Sa,Sb) OR Within(Sb,Sa))) AND Intersect(Sa,Sb))
         * Inclusion(Sa, Sb) = Within(Sa,Sb)

        """
        if len(self.all_geometry) < 0:
            raise ValueError("No geometry extracted. Check the `spatial_entities` arg during the initialization.")

        gdf_intersect = gpd.GeoDataFrame(self.all_geometry, columns="id geometry".split())
        for row in tqdm(gdf_intersect.itertuples(), total=len(gdf_intersect), desc="Computing intersections...", disable=(not self.verbose)):
            try:
                gdf_intersect["{0}".format(row.id)] = gdf_intersect.intersects(row.geometry)
            except Exception as e:
                print(e)

        gdf_within = gpd.GeoDataFrame(self.all_geometry, columns="id geometry".split())
        for row in tqdm(gdf_within.itertuples(), total=len(gdf_within), desc="Computing contains...", disable=(not self.verbose)):
            try:
                gdf_within["{0}".format(row.id)] = gdf_within.geometry.within(row.geometry)
            except Exception as e:
                print(e)

        gdf_intersect.set_index("id", inplace=True)
        gdf_within.set_index("id", inplace=True)
        del gdf_intersect["geometry"]
        del gdf_within["geometry"]

        corr_ = gdf_intersect ^ (
                    gdf_within | gdf_within.T)  # An entity cannot be related to an other entity by two type of relation
        adj_ = gdf_intersect & corr_  # because if include and not adjacent does not mean Adjacent !

        # Transform to dict for a fastest access !
        self.adjacency_geom = adj_
        self.inclusion_geom = gdf_within

    def get_relation_meta_based(self):
        """
        Extract relation between spatial entities based on their meta-data.

        """
        meta_adj_extractor = AdjacencyMetaRelation(self.spatial_entities)
        meta_inc_extractor = InclusionMetaRelation(self.spatial_entities)

        inc_res = {}
        for se1 in tqdm(self.spatial_entities, desc="Retrieve Inclusion based on meta_data", disable=(not self.verbose)):
            for se2 in self.spatial_entities:
                if not se1 in inc_res: inc_res[se1] = {}
                if meta_inc_extractor.is_relation(se1, se2):
                    inc_res[se1][se2] = True
                else:
                    inc_res[se1][se2] = False

        adj_res = {}
        for i in tqdm(range(len(self.spatial_entities)), desc="Retrieve Adjacency based on meta-data", disable=(not self.verbose)):
            se1 = self.spatial_entities[i]
            sub_spat = self.spatial_entities[i:len(self.spatial_entities)]
            for j in range(len(sub_spat)):
                se2 = sub_spat[j]
                if not se1 in adj_res: adj_res[se1] = {}
                if not se2 in adj_res: adj_res[se2] = {}
                adj_res[se1][se2] = meta_adj_extractor.is_relation(se1, se2)
                adj_res[se2][se1] = adj_res[se1][se2]

        self.adjacency_meta = pd.DataFrame.from_dict(adj_res)
        self.inclusion_meta = pd.DataFrame.from_dict(inc_res, orient="index")

    def fuse_meta_and_geom(self):
        """
        Merge relation found using two methods :
         * relation found using entities geometries
         * relation found using entities meta-data

        Returns
        -------
        tuple(pd.DataFrame, pd.DataFrame)
            adjacency relation dataframe, inclusion relation dataframe
        """
        # We sort both index and columns to apply logical combination correctly !
        self.adjacency_meta.sort_index(inplace=True)
        self.inclusion_meta.sort_index(inplace=True)
        self.adjacency_geom.sort_index(inplace=True)
        self.inclusion_geom.sort_index(inplace=True)
        self.adjacency_meta.sort_index(axis=1, inplace=True)
        self.inclusion_meta.sort_index(axis=1, inplace=True)
        self.adjacency_geom.sort_index(axis=1, inplace=True)
        self.inclusion_geom.sort_index(axis=1, inplace=True)

        df_inc = self.inclusion_meta
        self.adjacency_geom = (self.adjacency_geom ^ (
                    self.inclusion_meta | self.inclusion_meta.T)) & self.adjacency_geom
        self.adjacency_meta = (self.adjacency_meta ^ (
                    self.inclusion_meta | self.inclusion_meta.T)) & self.adjacency_meta
        df_adj = (self.adjacency_geom | self.adjacency_meta)

        return df_adj, df_inc


class AdjacencyMetaRelation(MetaCollector):

    def __init__(self, spatial_entities, verbose = False):
        """
        Constructor

        Parameters
        ----------
        spatial_entities : list
            list of spatial entities identifier
        """
        MetaCollector.__init__(self,verbose)
        self.p_47_dict = {}
        self.distances_is_inf_to = {}
        self.get_all_p47(spatial_entities)
        self.get_all_distances(spatial_entities)

    def get_all_p47(self, spatial_entities):
        """
        Extract P47 property value for each spatial entities in `ses`
        Parameters
        ----------
        spatial_entities : list
            list of spatial entities identifier

        """
        p47_dict = {}
        for es in tqdm(spatial_entities, desc="Extract all P47 data...", disable=(not self.verbose)):
            data = self.get_data(es)
            p47se1 = []
            if "P47" in data:
                for el in data.other.P47:
                    d = gazetteer.get_by_other_id(el, "wikidata")
                    if not d: continue
                    p47se1.append(d[0].id)
            p47_dict[data.id] = p47se1
        self.p_47_dict = p47_dict

    def get_all_distances(self, spatial_entities, max_d=1):
        """
        Compute the distance between all entities. Then associate a boolean value if a distance inferior to `max_d` value.
        Parameters
        ----------
        spatial_entities : list
            list of spatial entities

        """
        stop_class = {"A-PCLI", "A-ADM1"}  # Country or First Adminstrative cut

        data = {es: MetaCollector().get_data(es) for es in spatial_entities}

        coord = [MetaCollector().get_data(es) for es in spatial_entities]
        coord = [([c.id, Point(c.coord.lon, c.coord.lat)] if "coord" in c else [c.id, Point(180, 90)]) for c in coord]

        df = gpd.GeoDataFrame(coord, columns="id geometry".split())
        for row in tqdm(df.itertuples(), total=len(df), disable=(not self.verbose)):
            df["{0}".format(row.id)] = df.distance(row.geometry)

        dist_all = df.set_index("id").to_dict()

        for se1 in tqdm(spatial_entities, desc="Compute Distances", disable=(not self.verbose)):
            if not se1 in self.distances_is_inf_to: self.distances_is_inf_to[se1] = {}
            for se2 in spatial_entities:
                data_se1, data_se2 = data[se1], data[se2]
                if data_se1 and data_se2 and "coord" in data_se1 and "coord" in data_se2:
                    not_in_stop = len(set(data_se1.class_) & stop_class) < 0.5 and len(
                        set(data_se2.class_) & stop_class) < 0.5
                    self.distances_is_inf_to[se1][se2] = dist_all[se1][se2] < max_d and not_in_stop
                else:
                    self.distances_is_inf_to[se1][se2] = False

    def is_relation(self, id_se1: str, id_se2: str):
        if id_se1 in self.p_47_dict[id_se2]:
            return True

        elif id_se2 in self.p_47_dict[id_se1]:
            return True

        if self.distances_is_inf_to[id_se1][id_se2] or self.distances_is_inf_to[id_se1][id_se2]:
            return True
        return False


class InclusionMetaRelation(MetaCollector):
    _inc_chain_cache = {}

    def __init__(self, spatial_entities, verbose = False):
        """
        Constructor

        Parameters
        ----------
        spatial_entities : list
            list of spatial entities identifier
        """
        MetaCollector.__init__(self, verbose)
        self._inc_chain_cache = {}
        for se in tqdm(spatial_entities, desc="Extract Inclusion Chains", disable=(not self.verbose)):
            inc_chain_P131, inc_chain_P706 = self.get_inclusion_chain(se, "P131"), self.get_inclusion_chain(se, "P706")
            inc_chain = inc_chain_P131
            inc_chain.extend(inc_chain_P706)
            inc_chain = set(inc_chain)
            self._inc_chain_cache[se] = inc_chain

    def is_relation(self, id_se1: str, id_se2: str):

        if id_se1 in self._inc_chain_cache[id_se2]:
            return True

        return False

    def get_inclusion_chain(self, id_, prop):
        """
        For an entity return its geographical inclusion tree using a property.

        Parameters
        ----------
        id_ : str
            spatial entity identifier
        prop : str
            inclusion meta-data identifier

        Returns
        -------
        list
            inclusion chain
        """
        arr__ = []
        current_entity = gazetteer.get_by_id(id_)[0]
        if "inc_" + prop in current_entity.other:
            arr__ = current_entity.other["inc_" + prop]
        elif "inc_geoname" in current_entity.other:
            arr__ = current_entity.other.inc_geoname
        if isinstance(arr__, str):
            arr__ = [arr__]
        return arr__


def get_spatial_relations(spatial_entities,verbose = False):
    r = RelationExtractor(spatial_entities, verbose)
    r.get_relation_geometry_based()
    r.get_relation_meta_based()
    df_adj, df_inc = r.fuse_meta_and_geom()
    return objectify({"adjacency": df_adj.to_dict(), "inclusion": df_inc.to_dict()})
