# coding = utf-8
import re

import pandas as pd
import networkx as nx

from strpython.models.str import STR
from strpython.helpers.th_se_matcher import ThematicSpatialEntitiesMatcher


class ThematicSTR(STR):

    def __init__(self, text, spatial_entities, vocabulary_matcher=None, lang=None,already_built=False):
        STR.__init__(self, spatial_entities,toponym_first=False, already_built = already_built)
        self.text = text
        if vocabulary_matcher and lang:
            self.matcher = ThematicSpatialEntitiesMatcher(vocabulary_matcher, lang)
        self.graph = None
        self.thematic_entities = {}
        self.thematic_relationships= {}

    def __repr__(self):
        return STR.__repr__(self) + "\nThematic : {0}".format(self.thematic_entities)

    @staticmethod
    def from_networkx_graph(g: nx.Graph, tagged_: list = []):
        spatial_entities = {k:v for k,v in dict(g.nodes(data="label")).items() if k.startswith("GD")}
        STR_ = ThematicSTR("", spatial_entities,already_built=True)
        for src, tar, att in g.edges(data=True):
            if ("type_" in att and att["type_"] == "them") or ("color" in att and att["color"] == "blue"):
                STR_.add_thematic_relationships(src,tar)
                STR_.thematic_entities[tar]=g.node[tar]["label"]
        STR_.graph = g
        return STR_



    @staticmethod
    def from_STR(str_):

        str_t = ThematicSTR("", str_.spatial_entities)
        # To avoid unnecessary computations
        str_t.inclusion_relationships = str_.inclusion_relationships
        str_t.adjacency_relationships = str_.adjacency_relationships
        str_t.graph = str_.graph.copy()

        for node in str_t.graph.nodes:
            str_t.graph.nodes[node]["style"] = "filled"
            str_t.graph.nodes[node]["fontcolor"] = "white"
            str_t.graph.nodes[node]["fillcolor"] = "red"
            str_t.graph.nodes[node]["type"] = "S_E"
        return str_t

    def setup(self,text, vocabulary_matcher,lang):
        self.text = text
        self.matcher = ThematicSpatialEntitiesMatcher(vocabulary_matcher, lang)

    def add_thematic_entities(self, id, label):
        self.thematic_entities[id]=label

    def add_thematic_relationships(self,se,th):
        if not se in self.thematic_relationships:
            self.thematic_relationships[se] = {}
        self.thematic_relationships[se][th]= True

    def build(self, window_capture="window", window_size=5):
        res = []
        if window_capture == "window":
            res.append(self.matcher.within_window(self.text, self))
        if window_capture == "sentence":
            res.append(self.matcher.within_sentence(self.text, self))
        if len(res) < 1:
            return self
        res_fin = res[0]
        for r in res[1:]:
            res_fin = pd.concat((res_fin, r))

        spatial_entities_inv = {v: k for k, v in self.spatial_entities.items()}
        for i, row in res_fin.iterrows():
            for th in row.theme_in:
                label_i = self.matcher.terminology_.get_word(th)
                self.graph.add_node(th, label=label_i, style="filled", fontcolor="white", fillcolor="blue", type="T_E")
                self.thematic_entities[th] = label_i
        for i, row in res_fin.iterrows():
            for th in row.theme_in:
                for se in row.spat_in:
                    if se in spatial_entities_inv and not self.graph.has_edge(*(spatial_entities_inv[se], th)):
                        self.graph.add_edge(
                            spatial_entities_inv[se], th, color="blue",type_="them")
                        self.add_thematic_relationships(spatial_entities_inv[se], th)

        return self.graph

