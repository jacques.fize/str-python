# coding = utf-8
import copy
import os
import warnings


from tqdm import tqdm
import folium

import networkx as nx
import pandas as pd
import numpy as np

import geopandas as gpd
from shapely.geometry import MultiPoint, Polygon, Point, LineString

from sklearn.cluster import KMeans
from skcriteria.madm import simple
from skcriteria import Data, MIN

from ..helpers.geodict_helpers import gazetteer
from ..helpers.relation_cache import relation_cache
from .spatial_relation import get_spatial_relations

from joblib import Parallel,delayed
from strpython.helpers.collision import getGEO

import tempfile
from network2tikz import plot
from networkx.drawing.nx_agraph import graphviz_layout
import pyperclip

max_int = 1e6

def get_inclusion_chain(id_, prop):
    """
    For an entity return it geographical inclusion tree using a property.
    """
    arr__ = []
    current_entity = gazetteer.get_by_id(id_)[0]
    if "inc_" + prop in current_entity.other:
        arr__ = current_entity.other["inc_" + prop]
    elif "inc_geoname" in current_entity.other:
        arr__ = current_entity.other.inc_geoname
    if isinstance(arr__, str):
        arr__ = [arr__]
    return arr__


class STR(object):
    """
    Str basic structure
    """
    __cache_entity_data = {}  #  Store data about entity requested

    def __init__(self, spatial_entities, toponym_first=True, verbose=False,already_built=False):
        """
        Constructor

        Parameters
        ----------
        tagged_text : list
            Text in forms of token associated with tag (2D array 2*t where t == |tokens| )
        spatial_entities : dict
            spatial entities associated with a text. Follow this structure {"<id>: <label>"}

        """
        self.spatial_entities = spatial_entities
        if toponym_first:
            self.spatial_entities= {id_:topo for topo,id_ in self.spatial_entities.items()}

        for k in list(spatial_entities.keys()):
            if not k[:2] == "GD":
                del spatial_entities[k]

        if not already_built:
            self.load_spatial_relations()

        # Initialize Attributes
        self.adjacency_relationships = {}
        self.inclusion_relationships = {}
        self.graph = nx.MultiDiGraph()

        # Activate or Not Tqmd progress bar
        self.verbose = verbose

    def __repr__(self):
        return """STR\n\tSpatial Entities : {0}\n\tVerbose : {1}""".format(self.spatial_entities,self.verbose)

    def __contains__(self, item):
        return item in self.spatial_entities

    def load_spatial_relations(self):
        need_to_compute = False
        for s in self.spatial_entities:
            # If es not in cache
            if not relation_cache.inclusion.entity_exists(s) or not relation_cache.adjacency.entity_exists(s):
                need_to_compute = True
                break

        if need_to_compute:
            spatial_relation = get_spatial_relations(list(self.spatial_entities.keys()))
            relation_cache.update(spatial_relation.adjacency,spatial_relation.inclusion)




    @staticmethod
    def from_networkx_graph(g: nx.Graph, tagged_: list = []):
        """
        Build a STR based on networkx graph

        Parameters
        ----------
        g : nx.Graph
            input graph

        Returns
        -------
        STR
            resulting STR
        """

        sp_en = {}
        for nod in g:
            try:
                sp_en[nod] = g.node[nod]["label"]
            except KeyError:  # If no label found, grab one from the geo-database
                data = gazetteer.get_by_id(nod)
                if data:
                    sp_en[nod] = data[0].label

        str_ = STR(sp_en,toponym_first=False,already_built=True)
        str_.set_graph(g)
        return str_

    @staticmethod
    def from_dict(spat_ent: dict, toponym_first=True):
        """
        Build a STR based on networkx graph

        Parameters
        ----------
        toponym_first: bool
            if key is a toponym
        spat_ent : dict
            Dict of patial entities associated with a text. Follow this structure {"<id>: <label>"}
        Returns
        -------
        STR
            resulting STR
        """
        sp_en = {}
        for id_, label in spat_ent.items():
            sp_en[id_] = label

        str_ = STR(sp_en,toponym_first=toponym_first)
        str_.load_spatial_relations()
        str_.build()
        return str_

    @staticmethod
    def from_pandas(dataf: pd.DataFrame):
        """
        Build a STR from a Pandas Dataframe with two column : id and label.

        Parameters
        ----------
        dataf : pd.DataFrame
            dataframe containing the spatial entities
        tagged : list, optional
            tagged text (the default is []). A 2D array 2*t where t == |tokens|.

        Returns
        -------
        STR
            resulting STR
        """

        return STR.from_dict(pd.Series(dataf.label.values, index=dataf.id).to_dict())

    def set_graph(self, g):
        """
        Apply changes to the current STR based on Networkx Graph.

        Parameters
        ----------
        g : networkx.Graph
            input graph

        """

        self.graph = g
        rel_ = self.graph.edges(data=True)
        for edge in rel_:
            if edge[2]["color"] == "green":
                self.add_adjacency_rel(edge[0], edge[1])
            elif edge[2]["color"] == "red":
                self.add_inclusion_rel(edge[0], edge[1])

    def add_spatial_entity(self, id, label=None, v=True):
        """
        Add a spatial entity to the current STR

        Parameters
        ----------
        id : str
            identifier of the spatial entity in Geodict
        label : str, optional
            if not available in Geodict (the default is None)

        """
        data_ = self.get_data(id)
        if not data_:
            warnings.warn("{0} wasn't found in Geo-Database".format(id))
            return False
        if not label and v == True:
            warnings.warn("Label empty. @en label from Geo-Database will be used.")
            label = data_["en"]
        self.spatial_entities[id] = label
        self.graph.add_node(id, label=label,type="S_E")

    def remove_spatial_entity(self, id):
        """
        Add a spatial entity to the current STR

        Parameters
        ----------
        id : str
            identifier of the spatial entity in Geodict
        label : str, optional
            if not available in Geodict (the default is None)

        """
        if id in self.spatial_entities:
            del self.spatial_entities[id]
        if id in self.graph:
            self.graph.remove_node(id)

    def drop_zero_degree_entities(self):
        for n in list(self.spatial_entities.keys()):
            if self.graph.degree(n) <1:
                self.remove_spatial_entity(n)

    def add_spatial_entities(self, ids: list, labels: list = []):
        """
        Add spatial entities to the current STR

        Parameters
        ----------
        ids : list
            list of identifiers of each spatial entity
        labels : list, optional
            list of labels of each spatial entity

        """
        if not labels:
            warnings.warn("Labels list is empty. @en labels from Geo-Database will be used by default")
        for i in range(len(ids)):
            id = ids[i]
            try:
                label = labels[i]
            except:
                label = None
            self.add_spatial_entity(id, label, False)
        self.update()


    def add_adjacency_rel(self, se1, se2):
        """
        Add a adjacency relationship to the current STR.

        Parameters
        ----------
        se1 : str
            Identifier of the first spatial entity
        se2 : str
            Identifier of the second spatial entity

        """

        if not se1 in self.adjacency_relationships: self.adjacency_relationships[se1] = {}
        if not se2 in self.adjacency_relationships: self.adjacency_relationships[se2] = {}
        self.adjacency_relationships[se1][se2], self.adjacency_relationships[se2][se1] = True, True

    def add_inclusion_rel(self, se1, se2):
        """
        Add a inclusion relationship to the current STR.

        Parameters
        ----------
        se1 : str
            Identifier of the first spatial entity
        se2 : str
            Identifier of the second spatial entity

        """
        if not se1 in self.inclusion_relationships:
            self.inclusion_relationships[se1] = {}
        self.inclusion_relationships[se1][se2] = True




    def get_data(self, id_se):
        """
        Return an gazpy.Element object containing information about a spatial entity.

        Parameters
        ----------
        id_se : str
            Identifier of the spatial entity

        Returns
        -------
        gazpy.Element
            data
        """

        if id_se in STR.__cache_entity_data:
            return STR.__cache_entity_data[id_se]
        data = gazetteer.get_by_id(id_se)
        if len(data) > 0:
            STR.__cache_entity_data[id_se] = data[0]
            return data[0]

    def transform_spatial_entities(self, transform_map: dict):
        """
        Replace or delete certain spatial entities based on a transformation map

        Parameters
        ----------
        transform_map : dict
            New mapping for the spatial entities in the current STR. Format required : {"<id of the old spatial entity>":"<id of the new spatial entity>"}

        """

        final_transform_map = {}
        # Erase old spatial entities
        new_label = {}
        to_del = set([])
        for old_se, new_se in transform_map.items():
            data = self.get_data(new_se)
            to_del.add(old_se)
            if data:
                final_transform_map[old_se] = new_se
                if not new_se in self.spatial_entities:
                    self.add_spatial_entity(new_se, data.label.en)

                del self.spatial_entities[old_se]

                new_label[new_se] = data.label.en
            else:
                warnings.warn("{0} doesn't exists in the geo database!".format(new_se))

        self.graph = nx.relabel_nodes(self.graph, final_transform_map)

        for es in to_del:
            if es in self.graph._node:
                self.graph.remove_node(es)

        for se_ in new_label:
            if se_ in self.graph:
                self.graph.nodes[se_]["label"] = new_label[se_]
        self.update()

    def update(self):
        """
        Update the relationship between spatial entities in the STR. Used when transforming the STR.
        """

        nodes = []
        for k, v in self.spatial_entities.items():
            nodes.append((k, {"label": v,'type':"S_E"}))

        self.graph.clear()
        self.graph.add_nodes_from(nodes)

        # if new entities update
        self.load_spatial_relations()

        self.get_inclusion_relationships()
        for se1 in self.inclusion_relationships:
            for se2 in self.inclusion_relationships[se1]:
                if not se1 in self.graph.nodes or not se2 in self.graph.nodes:
                    continue
                if self.inclusion_relationships[se1][se2]:
                    self.graph.add_edge(se1, se2, key=0, color="red",type_="inc")

        self.get_adjacency_relationships()
        for se1 in self.adjacency_relationships:
            for se2 in self.adjacency_relationships[se1]:
                if not se1 in self.graph.nodes or not se2 in self.graph.nodes:
                    continue
                if self.adjacency_relationships[se1][se2]:
                    self.graph.add_edge(se1, se2, key=0, color="green",type_="adj")


    def get_inclusion_relationships(self):
        """
        Find all the inclusion relationships between the spatial entities declared in the current STR.

        """
        for se_ in tqdm(self.spatial_entities, desc="Extract Inclusion", disable=(not self.verbose)):
            for se2_ in self.spatial_entities:
                if se_ != se2_ and relation_cache.inclusion.is_relation(se_,se2_):
                    self.add_inclusion_rel(se_, se2_)

    def get_adjacency_relationships(self):
        """
        Find all the adjacency relationships between the spatial entities declared in the current STR.
        """

        for se1 in tqdm(self.spatial_entities, desc="Extract Adjacency Relationship", disable=(not self.verbose)):
            for se2 in self.spatial_entities:
                if se1 != se2 and relation_cache.adjacency.is_relation(se1, se2):
                    self.add_adjacency_rel(se1,se2)

    def build(self, inc=True, adj=True, verbose=False):
        """
        Build the STR

        Parameters
        ----------
        inc : bool, optional
            if inclusion relationship have to be included in the STR (the default is True)
        adj : bool, optional
            if adjacency relationship have to be included in the STR (the default is True)
        verbose : bool, optional
            Verbose mode activated (the default is False)

        Returns
        -------
        networkx.Graph
            graph representing the STR
        """

        # Initialize Graph
        nodes = []
        for k, v in self.spatial_entities.items():
            nodes.append((k, {"label": v,"type":"S_E"}))
        graph = nx.MultiDiGraph()
        graph.add_nodes_from(nodes)

        self.get_adjacency_relationships()
        for se1 in self.adjacency_relationships:
            for se2 in self.adjacency_relationships[se1]:
                if self.adjacency_relationships[se1][se2]:
                    graph.add_edge(se1, se2, key=0, color="green", type_="adj")
                    graph.add_edge(se2, se1, key=0, color="green", type_="adj")

        self.get_inclusion_relationships()
        for se1 in self.inclusion_relationships:
            for se2 in self.inclusion_relationships[se1]:
                if self.inclusion_relationships[se1][se2]:
                    graph.add_edge(se1, se2, key=0, color="red", type_="inc")

        self.graph = graph
        return graph

    def get_undirected(self,simple_graph=True):
        """
        Return the Undirected form of a STR graph.

        Returns
        -------
        networkx.Graph
            unidirected graph
        """
        if simple_graph:
            return  nx.Graph(self.graph)
        return nx.MultiGraph(self.graph)

    def get_geo_data_of_se(self):
        """
        Return Geographical information for each spatial entities in the STR

        Returns
        -------
        geopandas.GeoDataFrame
            dataframe containing geographical information of each entity in the STR
        """

        points, label, class_ = [], [], []
        for se in self.spatial_entities:
            data = gazetteer.get_by_id(se)[0]
            try:
                points.append(Point(data.coord.lon, data.coord.lat))
                label.append(data.label)
                # class_.append(most_common(data["class"]))
            except KeyError:
                pass
        # print(len(points),len(label),len(class_))
        df = gpd.GeoDataFrame({"geometry": points, "label": label})
        df["x"] = df.geometry.apply(lambda p: p.x)
        df["y"] = df.geometry.apply(lambda p: p.y)
        return df

    def get_geo_dissolved(self):
        es = [getGEO(en) for en in self.spatial_entities]
        es = [[1, e.values[0][0]] if isinstance(e, gpd.GeoDataFrame) else [1, e.values[0]] for e in es if
              isinstance(e, gpd.GeoDataFrame) or isinstance(e, gpd.GeoSeries)]
        return gpd.GeoDataFrame(es, columns="dd geometry".split()).dissolve(by="dd")

    def get_cluster(self, id_=None):
        """
        Return the cluster detected using spatial entities position.

        Parameters
        ----------
        id_ : temp_file_id, optional
            if cached version of geoinfo (the default is None)

        Returns
        -------
        gpd.GeoDataFrame
            cluster geometry
        """

        if os.path.exists("./temp_cluster/{0}.geojson".format(id_)):
            return gpd.read_file("./temp_cluster/{0}.geojson".format(id_))
        if len(self)<1:
            return gpd.GeoDataFrame()
        try:
            gdf = self.get_geo_data_of_se()

            points = gdf.geometry.apply(lambda x: (x.x, x.y)).tolist()
            opt_n_cluster = self.best_model_kmeans(points)
            gdf["cluster"] = KMeans(n_clusters=opt_n_cluster, random_state=1).fit(points).labels_#kmeans2(points, opt_n_cluster, iter=10, minit='points')[1]
            tt = gdf.groupby("cluster").apply(to_Polygon)
            clusters_ = gpd.GeoDataFrame(
                np.concatenate((np.array(tt.index.tolist()).reshape(-1, 1), np.array(tt.tolist())), axis=1),
                columns="cluster geometry nb_point".split())

            def treatment(cluster):
                cluster["dd"] = np.ones(len(cluster))
                cluster= cluster.dissolve(by="dd").explode().reset_index()
                cluster["cluster"] = np.arange(len(cluster))
                return cluster

            clusters_ = treatment(clusters_)
            if id_:
                clusters_.to_file("./temp_cluster/{0}.geojson".format(id_))
        except Exception as e:
            clusters_=gpd.GeoDataFrame()


        return clusters_

    def best_model_kmeans(self,points, max_n=25):
        data = []
        def kmeans(k, x):
            try:
                return KMeans(n_clusters=k, random_state=1).fit(x)
            except:
                return None

        models = Parallel(n_jobs=1)(delayed(kmeans)(k, points) for k in range(2, max_n))
        for k in range(len(models)):
            if models[k]:
                # Sum of distances of samples to their closest cluster center
                initeria = models[k].inertia_
                data.append([initeria, np.bincount(models[k].labels_)[1]])
            else:
                data.append([max_int,max_int])

        dd = Data(data, criteria=[MIN, MIN], anames=list(range(2, max_n)))
        return simple.WeightedSum().decide(dd).best_alternative_

    def to_folium(self):
        """
        Use the folium package to project the STR on a map

        Returns
        -------
        folium.Map
            folium map instance
        """

        points = []
        for se in self.spatial_entities:
            data = gazetteer.get_by_id(se)[0]
            try:
                points.append(Point(data.coord.lon, data.coord.lat))
            except:
                pass

        lines_adj = []
        for se1 in self.adjacency_relationships:
            data_se1 = gazetteer.get_by_id(se1)[0]
            for se2 in self.adjacency_relationships[se1]:
                data_se2 = gazetteer.get_by_id(se2)[0]
                if self.adjacency_relationships[se1][se2]:
                    lines_adj.append(
                        LineString([(data_se1.coord.lon, data_se1.coord.lat),
                                    (data_se2.coord.lon, data_se2.coord.lat)])
                    )
        lines_inc = []
        for se1 in self.inclusion_relationships:
            data_se1 = data_se1 = gazetteer.get_by_id(se1)[0]
            for se2 in self.inclusion_relationships[se1]:
                if self.inclusion_relationships[se1][se2]:
                    data_se2 = gazetteer.get_by_id(se2)[0]
                    lines_inc.append(
                        LineString([
                            (data_se1.coord.lon, data_se1.coord.lat),
                            (data_se2.coord.lon, data_se2.coord.lat)]
                        )
                    )

        def to_fol(seris, color="#ff0000"):
            df = gpd.GeoDataFrame(geometry=seris.values)
            df.crs = {'init': 'epsg:4326'}
            return folium.features.GeoJson(df.to_json(), style_function=lambda x: {'color': color})

        gjson1 = to_fol(gpd.GeoSeries(points))
        gjson2 = to_fol(gpd.GeoSeries(lines_adj), color='#00ff00')
        gjson3 = to_fol(gpd.GeoSeries(lines_inc))

        map = folium.Map()
        map.add_child(gjson1)
        map.add_child(gjson2)
        map.add_child(gjson3)

        return map

    def map_projection(self, plt_=False,figsize=(16, 9)):
        """
        Return a matplotlib figure of the STR

        Parameters
        ----------
        plt : bool, optional
            if the user wish to use the plt.show() (the default is False)

        Returns
        -------
        plt.Figure
            Matplotlib figure instance
        """

        import matplotlib.pyplot as plt
        world = gpd.read_file(gpd.datasets.get_path('naturalearth_lowres'))
        world.to_crs(epsg=3857)
        base = world.plot(color='white',edgecolor='black', figsize=figsize)
        points = []
        for se in self.spatial_entities:
            data = gazetteer.get_by_id(se)[0]
            try:
                points.append(Point(data.coord.lon, data.coord.lat))
            except:
                pass

        lines_adj = []
        for se1 in self.adjacency_relationships:
            data_se1 = gazetteer.get_by_id(se1)[0]
            for se2 in self.adjacency_relationships[se1]:
                data_se2 = gazetteer.get_by_id(se2)[0]
                if self.adjacency_relationships[se1][se2]:
                    lines_adj.append(
                        LineString([(data_se1.coord.lon, data_se1.coord.lat), (data_se2.coord.lon, data_se2.coord.lat)])
                    )
        lines_inc = []
        for se1 in self.inclusion_relationships:
            data_se1 = gazetteer.get_by_id(se1)[0]
            for se2 in self.inclusion_relationships[se1]:
                if self.inclusion_relationships[se1][se2]:
                    data_se2 = gazetteer.get_by_id(se2)[0]
                    lines_inc.append(
                        LineString([
                            (data_se1.coord.lon, data_se1.coord.lat),
                            (data_se2.coord.lon, data_se2.coord.lat)]
                        )
                    )

        gpd.GeoSeries(points).plot(ax=base, marker='o', markersize=5, color="blue")
        gpd.GeoSeries(lines_adj).plot(ax=base, color="green")
        gpd.GeoSeries(lines_inc).plot(ax=base, color="red")
        if not plt_:
            return base
        plt.show()


    def plot(self, title="STR", output_fn=None,se_color ="#4183d7",te_color="#d64541",inc_edge_color="r",
             adj_edge_color="g",them_edge_color = "b",figsize=(7,7),scale=2,node_size=700,layout_func=nx.shell_layout,dech=0):

        import matplotlib.pyplot as plt
        plt.figure(figsize=figsize)
        G = self.graph.copy()
        try:
            pos = layout_func(G, scale=scale)
        except:
            pos = layout_func(G)
        #pos = nx.layout.shell_layout(ext_2_t.graph)

        nodes = list(G.nodes(data=True))
        max_n_char= ([len(n[1]["label"]) * node_size for n in nodes])

        nx.draw_networkx_nodes(G, pos,
                            nodelist=[n[0] for n in nodes if n[1]["type"] == "S_E"],
                            node_color=se_color, node_size=max_n_char)

        nx.draw_networkx_nodes(G, pos,
                                nodelist=[n[0] for n in nodes if n[1]["type"] == "T_E"],
                                node_color=te_color, node_size=max_n_char)

        edges = list(G.edges(data=True))
        nx.draw_networkx_labels(G, pos, labels={n[0]: n[1]["label"] for n in nodes},font_color='w')
        nx.draw_networkx_edges(G, pos, edgelist=[ed for ed in edges if ed[2]["type_"] == "inc"],
                               edge_color=inc_edge_color, arrows=True,width=1.5)
        nx.draw_networkx_edges(G, pos, edgelist=[ed for ed in edges if ed[2]["type_"] == "adj"],
                               edge_color=adj_edge_color, arrows=True,width=1.5)
        nx.draw_networkx_edges(G, pos, edgelist=[ed for ed in edges if ed[2]["type_"] == "them"],
                               edge_color=them_edge_color, arrows=True,width=1.5)

        plt.title(title)
        plt.axis('off')
        plt.margins(0.2)
        if output_fn:
            plt.savefig(output_fn, bbox_inches='tight')
        else:
            return plt.gca()

    def to_latex(self,to_clipboard=False,geo_layout=False):

        def get_color(x):
            if x == "S_E":
                return "blue_tikznetw"
            else:
                return "red_tikznetw"

        color_data ="""\\usepackage{xcolor}\n\\definecolor{red_tikznetw}{HTML}{D91E18}\n\\definecolor{blue_tikznetw}{HTML}{4183D7}"""

        G =self.graph.copy()
        fn = tempfile.NamedTemporaryFile().name

        pos = graphviz_layout(G)

        if geo_layout:
            pos={}
            for se in self.spatial_entities:
                data = gazetteer.get_by_id(se)[0]
                pos[se] = (data.coord.lon, data.coord.lat)


        plot(G,
             filename=fn,
             type="tex",
             layout=pos,
             vertex_label=dict(G.nodes(data="label")),
             vertex_color = {k:get_color(v) for k,v in dict(G.nodes(data="type")).items()},
             edge_color=[ed[-1] for ed in list(G.edges(data="color"))],
             vertex_label_position='below',
             canvas=(10, 10),
             node_label_style="{\\bfseries}"
             )
        tex_data = open(fn).read().split("\n")
        tex_data.insert(2,color_data)
        tex_data = "\n".join(tex_data)

        if not to_clipboard:
            return tex_data
        pyperclip.copy(tex_data)
    def __len__(self):
        return len(self.spatial_entities)


def to_Polygon(x):
    """
    Return a polygon buffered representation for a set of points.

    Parameters
    ----------
    x : pandas.Series
        coordinates columns

    Returns
    -------
    shapely.geometry.Polygon
        polygon
    """
    points = [Point(z) for z in x[["x", "y"]].values]
    if len(points) > 2:
        coords = [p.coords[:][0] for p in points]
        return [Polygon(coords).buffer(1).convex_hull,len(coords)]
    elif len(points) == 1:
        return [points[0].buffer(1),1]
    else:
        coords = [p.coords[:][0] for p in points]
        return [LineString(coords).buffer(1).convex_hull,len(coords)]