# coding = utf-8
import copy
import warnings

import networkx as nx
import numpy as np
from elasticsearch import Elasticsearch

from ...config.configuration import config
from ...helpers.geodict_helpers import gazetteer
from ..str import STR, get_inclusion_chain

client = Elasticsearch(config.es_server)


class Transformation():
    """
    Transform class basic structure
    """

    def transform(self, str_: STR, **kwargs) -> STR:
        """
        Transform a STR
        :param str_: STR input
        :param kwargs: dict --> args needed for the transformation
        :return: STR
        """
        pass


class Generalisation(Transformation):
    """
    Generalisation transformation class declaration. Basically, the generalisation will replace spatial entity in a STR,
    by its "upper" entity -- an upper entity e1 of e2, includes e2-- using different hypothesis. The first hypothesis
    "transform_all", generalise all the spatial entity, while the second "transformation_bounded" only transform entities
    between a certain level (town, country, region, ...).
    """

    # Level accepted for "transform_bounded()"
    bounded_class_references = {
        "country": ["A-PCLI"],
        "region": ["A-ADM1"],
        #"capital": ["P-PPLC"],
        "town": ["A-ADM4", "P-PPL"]
    }

    def transform(self, str_: STR, **kwargs) -> STR:
        """

        :param str_: STR
        :param kwargs: args
        :return: STR
        """
        h = kwargs.get("n", 1)
        type_ = kwargs.get("type_gen", "all")
        bound = kwargs.get("bound", "country")
        cp = kwargs.get("cp", True)
        """
        Store Inclusion Informations
        """
        if type_ == "all":
            return self.transform_all(str_, h, cp=cp)
        if type_ == "bounded":
            return self.transform_bounded(str_, bound, cp=cp)
        else:
            warnings.warn("No Generalisation transform type specified! Using \"all\" generalisation by default")
            return self.transform_all(str_, h, cp=cp)

    @staticmethod
    def get_inclusion_map(graph: nx.Graph):
        inclusion_dictionnary = {}
        for node in graph.nodes():
            if not node in inclusion_dictionnary:
                inc_list = []
                data = gazetteer.get_by_id(node)
                if len(data)<1:continue
                data=data[0]
                try:
                    inc_list = data.other.inc_P131
                except:
                    pass
                if not inc_list:
                    if "inc_geoname" in data.other:
                        inc_list = data.other.inc_geoname
                if inc_list:
                    inc_list = inc_list if isinstance(inc_list, list) else [inc_list]

                    inclusion_dictionnary[node] = inc_list
        return inclusion_dictionnary

    def get_associated_classes(self, inclusion_dict: dict):
        associated_classes = {}
        for es, inc_chain in inclusion_dict.items():
            list_ = [es]
            list_.extend(inc_chain)
            for it in list_:
                if not it in associated_classes:
                    try:
                        classes_list = gazetteer.get_by_id(it)[0].class_
                    except:
                        print("No class found for {0}".format(it))
                        continue
                    classes_list = classes_list if isinstance(classes_list, list) else [classes_list]
                    associated_classes[it] = classes_list
        return associated_classes

    def transform_bounded(self, str_: STR, bound: str, cp=True) -> STR:
        if not bound in Generalisation.bounded_class_references:
            print("'bound' must be a value from {0}".format(str(Generalisation.bounded_class_references)))
            exit()
        graph = str_.graph
        classes_codes_authorized = Generalisation.bounded_class_references[bound]
        inclusion_map = Generalisation.get_inclusion_map(graph)
        classes_map = self.get_associated_classes(inclusion_map)

        transform_map = {}
        for es, inc_chain in inclusion_map.items():
            t_ = ""
            for es_i in inc_chain:
                if es_i in classes_map:
                    if len(set(classes_map[es_i]).intersection(set(classes_codes_authorized))) > 0:
                        t_ = es_i
            if t_:
                transform_map[es] = t_
        if cp:
            copy_ = copy.deepcopy(str_)
            copy_.transform_spatial_entities(transform_map)
            copy_.update()
            return copy_

        str_.transform_spatial_entities(transform_map)
        str_.update()
        return str_

    def transform_all(self, str_: STR, h: int, cp=True) -> STR:
        h = int(h)
        graph = str_.graph
        inclusion_dict = Generalisation.get_inclusion_map(graph)
        transform_map = {}
        new_label = {}
        i = 0
        for node in graph.nodes():
            if node in inclusion_dict:
                inc_chain = inclusion_dict[node]
                if len(inc_chain) < h:
                    transform_map[node] = inc_chain[-1]  # if h superior to chain lenght
                    new_label[inc_chain[-1]] = gazetteer.get_by_id(inc_chain[-1])[0].label.en
                else:
                    transform_map[node] = inc_chain[h - 1]
                    new_label[inc_chain[h - 1]] = gazetteer.get_by_id(inc_chain[h - 1])[0].label.en
        if cp:
            copy_ = copy.deepcopy(str_)
            copy_.transform_spatial_entities(transform_map)
            copy_.update()
            return copy_
        str_.transform_spatial_entities(transform_map)
        str_.update()
        return str_


class Expansion(Transformation):
    def getAroundEntities(self, data, score, distance=100, unit="km", n=1,lang="fr",stop_en=[]):
        if not "coord" in data:
            return []
        hits = client.search("gazetteer", "place", {
            "query": {
                "bool": {
                    "must": [
                        {"match_all": {}},
                        {"exists": {"field": "score"}},  # Get place with high score
                        #{"terms": {"class": ["P-PPL", "A-ADM4", "P-PPLC"]}},
                        # Populated Settlement, Last administration level, Capital
                        {"range": {"score": {"gt": score}}},  # Has a higher score (PR)
                        #{"term": {"country": data.other["country"]}}  # stay in the same country
                    ],
                    "must_not": [
                        {"terms": {"class": ["A-ADM3", "A-ADM2", "A-ADM1"]}},
                        {"terms": {lang: stop_en}},
                    ],
                    "filter": {
                        "geo_distance": {
                            "distance": "{0}{1}".format(distance, unit),
                            "coord": data.coord
                        }
                    }
                }
            }
            , "sort": [
                {"score": "desc"}
            ], "size": n})

        if hits["hits"]["total"] > 0:
            ids_ = []
            for h in hits["hits"]["hits"]:
                ids_.append(h["_source"]["id"])
            return ids_
        return []

    def select_es(self, graph):
        es = np.array(list(graph.nodes()))
        score = [-1 for i in range(len(es))]
        for e in range(len(es)):
            data = gazetteer.get_by_id(es[e])
            if len(data) >0:
                score[e] = float(data[0].score)
        return np.median(score), es[score < np.median(score)].tolist()

    def transform(self, str_: STR, **kwargs):
        type_ = "adjacency"
        distance = kwargs.get("distance", 100)
        unit = kwargs.get("unit", 100)
        n = kwargs.get("adjacent_count", 1)
        cp = kwargs.get("cp", True)
        lang = kwargs.get("lang","fr")
        if type_ == "adjacency":
            return self.transform_adj(str_, distance, unit, n, lang, cp)

    def transform_adj(self, str_: STR, distance: int, unit: str, n: int,lang:str, cp=True) -> STR:
        graph = str_.graph
        median, selected_se = self.select_es(graph)
        data_se, scores_ = {}, []
        for node in list(selected_se):
            d_=gazetteer.get_by_id(node)
            if len(d_) >0:
                data_se[node] = d_[0]
                if "score" in data_se[node] :
                    scores_.append(data_se[node].score)
                else:
                    scores_.append(-1)
            else:
                selected_se.remove(node)

        new_nodes = []
        labels = []
        stop_en = list(str_.spatial_entities.keys())
        for node in selected_se:
            data_ = data_se[node]
            if (not "P-PPL" in data_.class_) and (not "A-ADM4" in data_.class_):
                continue
            if not "country" in data_.other:
                continue
            neighbor = self.getAroundEntities(data_, median, distance, unit, n,lang=lang,stop_en=stop_en)
            #stop_en.extend(neighbor)
            # if not neighbor:
            #     try:
            #         neighbor = [get_inclusion_chain(node, "P131")[0]]
            #     except:
            #         neighbor = []
            labels.extend([gazetteer.get_by_id(n)[0].label[lang] for n in neighbor])
            new_nodes.extend(neighbor)

        new_nodes = list(set(new_nodes))
        if cp:
            copy_ = copy.deepcopy(str_)
            copy_.add_spatial_entities(new_nodes, labels)

            copy_.update()
            return copy_

        str_.add_spatial_entities(new_nodes, labels)
        str_.update()
        return str_
