# coding = utf-8

import pandas as pd
from strpython.models.str import STR
from ..thematic_str import ThematicSTR

from ..spatial_relation import get_spatial_relations
from ...helpers.relation_cache import relation_cache

def get_generalized_with_thematic( generalised_str: STR, str_thematic :ThematicSTR):

    thematic_rel = str_thematic.thematic_relationships

    new_gen = ThematicSTR.from_networkx_graph(generalised_str.graph)

    relation_cache.update(new_gen.adjacency_relationships, new_gen.inclusion_relationships)
    relation_cache.update(str_thematic.adjacency_relationships, str_thematic.inclusion_relationships)
    linked_es = set(list(thematic_rel.keys()))  #  Spatial entities linked to Thematic Unit

    all_es = list(set(generalised_str.spatial_entities.keys()) | set(str_thematic.spatial_entities.keys()))
    for es in all_es:
        for es2 in all_es:
            if es != es2 and not (relation_cache.inclusion.in_cache(es,es2)[0] and relation_cache.inclusion.in_cache(es2,es)[0]):
                data = get_spatial_relations(all_es)
                relation_cache.update(data.adjacency, data.inclusion)
                break

    for old_es in linked_es:
        if old_es in new_gen:  # If still in the generalised graph, connect it to the thematic unit
            for them in thematic_rel[old_es]:
                if not them in new_gen.thematic_entities:
                    new_gen.add_thematic_entities(them,str_thematic.thematic_entities[them])
                    new_gen.graph.add_node(them, label=str_thematic.thematic_entities[them], type="T_E")
                new_gen.graph.add_edge(old_es, them, color="blue",type_="them")
                new_gen.add_thematic_relationships(old_es, them)
            continue

        for new_es in generalised_str.spatial_entities:  # Recherche dans les noeuds de la version généralisé
            if relation_cache.inclusion.is_relation(old_es,new_es) and not new_es in str_thematic:  # si le noeud du STR original se trouve inclus dans un nouveau #data.inclusion[old_es][new_es]
                for them in thematic_rel[old_es]:
                    if not them in new_gen.thematic_entities:
                        new_gen.add_thematic_entities(them, str_thematic.thematic_entities[them])
                        new_gen.graph.add_node(them, label=str_thematic.thematic_entities[them], type="T_E")
                    if not new_gen.graph.has_edge(new_es, them):
                        new_gen.graph.add_edge(new_es, them, color="blue",type_="them")
                        new_gen.add_thematic_relationships(new_es, them)

    return new_gen


def get_extended_with_thematic(extended_str, thematic_str):
    new_ext = ThematicSTR.from_networkx_graph(extended_str.graph)
    thematic_rel = thematic_str.thematic_relationships
    for es in thematic_rel:
        for them in thematic_rel[es]:
            if not them in new_ext.thematic_entities:
                new_ext.add_thematic_entities(them, thematic_str.thematic_entities[them])
                new_ext.graph.add_node(them, label=thematic_str.thematic_entities[them], type="T_E")
            print(es,them)
            new_ext.graph.add_edge(es, them, color="blue",type_="them")
            new_ext.add_thematic_relationships(es, them)
    return new_ext
