# coding = utf-8
import argparse

from strpython.nlp.ner.spacy import Spacy as spacy_ner
from strpython.nlp.ner.polyglot import Polyglot as poly_ner
from strpython.nlp.ner.stanford_ner import StanfordNER as stanford_ner

from strpython.nlp.disambiguator.wikipedia_cooc import WikipediaDisambiguator as wiki_d
from strpython.nlp.disambiguator.share_prop import ShareProp as shared_geo_d
from strpython.nlp.disambiguator.most_common import MostCommonDisambiguator as most_common_d
import sys

disambiguator_dict = {
        "occwiki": wiki_d,
        "most_common": most_common_d,
        "shareprop": shared_geo_d
    }

ner_dict = {
    "spacy": spacy_ner,
    "polyglot":poly_ner,
    "stanford": stanford_ner
}

def get_parser_generate_str():


    help_input = """Filename of your input. Must be in Pickle format with the following columns :
      - filename : original filename that contains the text in `content`
      - id_doc : id of your document
      - content : text data associated to the document
      - lang : language of your document 
    """

    parser = argparse.ArgumentParser(formatter_class=argparse.RawTextHelpFormatter)

    # REQUIRED
    parser.add_argument("input_pkl", help=help_input)
    # OPTIONAL
    parser.add_argument("-n", "--ner",
                        help="The Named Entity Recognizer you wish to use",
                        choices=list(ner_dict.keys()),
                        default="spacy")
    parser.add_argument("-d", "--disambiguator",
                        help="The Named Entity disambiguator you wish to use",
                        choices=list(disambiguator_dict.keys()),
                        default="most_common")
    parser.add_argument("-t", "--transform",
                        help="Transformation to apply",
                        action="append",
                        choices=["gen", "ext"])

    parser.add_argument("-o", "--output",
                        help="Output Filename",
                        default="output.pkl"
                        )



class JsonProgress(object):
    def __init__(self,fn):
        self.count = 0
        self.fn= fn
    def __call__(self, obj):
        self.count += 1
        if self.count %10 == 0:
            sys.stdout.write("\rLoading"+self.fn+": %8d" % self.count)
        return obj
