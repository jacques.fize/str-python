import json
import os
import textwrap

import community  # community partition
import networkx as nx
import numpy as np
from tqdm import tqdm

from ..helpers.sim_matrix import read_bz2_matrix
from .utils import discrete_cmap


def get_intersect(id_1, id_2, graph_dir):
    """
    Get nodes intersections between two graphs
    Parameters
    ----------
    id_1
    id_2
    graph_dir

    Returns
    -------

    """
    try:
        G = nx.read_gexf(os.path.join(graph_dir, "{0}.gexf".format(id_1)))
    except FileNotFoundError:
        G = nx.DiGraph()
    try:
        H = nx.read_gexf(os.path.join(graph_dir, "{0}.gexf".format(id_2)))
    except FileNotFoundError:
        H = nx.DiGraph()
    ids = list(set(G.nodes()).intersection(set(H.nodes())))
    dict_entity = {}
    for id_ in ids:
        if id_ in G:
            dict_entity[id_] = G.nodes[id_]["label"]
        else:
            dict_entity[id_] = H.nodes[id_]["label"]
    return dict_entity


def parse_matrix(input_matrix_fn, selected_fn, label_assoc_dict, topn=5):
    """

    Parameters
    ----------
    input_matrix_fn
    selected_fn
    label_assoc_dict
    topn

    Returns
    -------

    """
    sim_matrix = read_bz2_matrix(input_matrix_fn)
    min_ = np.min(sim_matrix)
    if min_ < 0:
        sim_matrix = (sim_matrix - sim_matrix.min()) / (sim_matrix.max() - sim_matrix.min())

    selected = json.load(open(selected_fn))

    G = nx.Graph()
    maxi = np.max(sim_matrix)
    for doc_ in tqdm(selected):
        mask = np.argsort(sim_matrix[doc_])[::-1][1:topn + 1]
        weights = sim_matrix[doc_][mask]
        for ix, item in enumerate(mask):
            if not doc_ == mask[ix]:
                G.add_edge(str(doc_), str(mask[ix]), weight=weights[ix] / maxi,
                           intersect_entity=get_intersect(doc_, ix))
    for node_ in list(G.nodes()):
        G.nodes[node_]["label"] = textwrap.fill(label_assoc_dict[node_].replace("/Users/jacquesfize/LOD_DATASETS/", ""),
                                                width=25)
    for node_ in list(G.nodes()):
        if G.degree(node_) < topn:
            G.remove_node(node_)

    communities = community.best_partition(G)
    key_com = set(communities.values())
    N = len(key_com)
    colors = discrete_cmap(N, "Set3")
    for node_, com_index in communities.items():
        G.nodes[node_]["color"] = colors[com_index]

    return G
