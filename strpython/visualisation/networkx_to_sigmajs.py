import json
import math

import networkx as nx

from .utils import discrete_cmap


def get_colors_index(weight_value, min_weight, max_weight, len_color_list):
    """
    Get colors index for a weight value
    Parameters
    ----------
    weight_value
    min_weight
    max_weight

    Returns
    -------

    """
    index = math.floor(((weight_value - min_weight) / (max_weight - min_weight)) * len_color_list)
    if index ==len_color_list:
        return len_color_list-1
    return index


def export_to_sigmajs_json(G, output_path, edge_colors = discrete_cmap(100, "jet"), node_colors = discrete_cmap(100, "jet"), **kwargs):
    """
    Write SigmaJs Json format of a graph
    Parameters
    ----------
    G : nx.Graph
        graph
    output_path : str
        output path
    edge_colors: list
        colors that will be used for edge (based on weight)
    node_colors : list
        list of colors that will be used for the node (if pagerank is true)
    pagerank : bool
        if compute pagerank
    layout : networkx layout function
        function used to decide of the nodes positions (default is networkx.spring_layout)
    Returns
    -------

    """
    layout_function = kwargs.get("layout", nx.spring_layout)
    pagerank = kwargs.get("pagerank", True)

    nodes_positions = layout_function(G)
    pr, min_weight, max_weight = {}, 1, 1

    if pagerank:
        pr = nx.pagerank(G, alpha=0.85)
        min_weight = min(pr.values())
        max_weight = max(pr.values())

    nodes = [{"id": str(k),
              "label": str(G.nodes[k]["label"]).split('/')[-1],
              "size": str(100*pr[k]) if pagerank else 30,
              "color":node_colors[get_colors_index(pr[k], min_weight, max_weight, len(node_colors))] if pagerank else "#eee",
              "x": nodes_positions[k][0],
              "y": nodes_positions[k][1]}
             for k in nodes_positions]
    try:
        max_weight = max([e[2]["weight"] for e in G.edges(data=True)])
        min_weight = min([e[2]["weight"] for e in G.edges(data=True)])
    except KeyError:
        print("No weight found on edges, assign default value")
        min_weight, max_weight = 0.1, 0.1

    edges = [{"id": '%s-%s' % (e[0], e[1]),
              "source": e[0],
              "target": e[1],
              "size": e[2]["weight"]*100,
              "color":edge_colors[get_colors_index(e[2]["weight"], min_weight, max_weight, len(edge_colors))],
              "label": "|".join(e[2]["intersect_entity"].values()) if "intersect_entity" in e[2] else "",
              "data":e[2]}
             for e in G.edges(data=True)]
    try:
        with open(output_path, 'w') as f:
            json.dump({"nodes": nodes, "edges": edges}, f)
    except Exception as e:
        print("Error while saving JSON : {0}".format(e.__repr__()))
