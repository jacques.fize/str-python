# coding = utf-8

import matplotlib
import numpy as np


def discrete_cmap(N, base_cmap="tab10"):
    """
    Create an N-bin discrete colormap from the specified input map
    """
    base = matplotlib.pyplot.get_cmap(base_cmap)
    color_list = base(np.linspace(0, 1, N))

    return [matplotlib.colors.rgb2hex(i[:3]) for i in color_list]
