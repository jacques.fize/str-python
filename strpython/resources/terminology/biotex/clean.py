import argparse, re

import nltk
from nltk.stem import WordNetLemmatizer
from french_lefff_lemmatizer.french_lefff_lemmatizer import FrenchLefffLemmatizer
wordnet_lemmatizer = WordNetLemmatizer()
french_lem = FrenchLefffLemmatizer()

import pandas as pd


parser = argparse.ArgumentParser()
parser.add_argument("input")
parser.add_argument("lang")
parser.add_argument("output")

args = parser.parse_args()

def lemmatize(token, lang):
    if lang == "en":
        return wordnet_lemmatizer.lemmatize(token)
    else:
        return french_lem.lemmatize(token)

df = pd.read_csv(args.input)
if 'label' in df and not "term" in df:
    df["term"]=df["label"]


df["lem"]=df.term.apply(lambda x: " ".join([lemmatize(str(token),args.lang) for token in str(x).split(" ")]))
df["matched"] = df.term.apply(lambda x: True if re.match("[\w\s\d\-\']+",str(x)) else False)

df = df[df.matched].drop_duplicates("lem")

if not "gram" in df:
    df["gram"] = df.term.apply(lambda x : len(str(x).split(" ")))

pd.concat((
    df[df.gram == 1].sort_values("rank",ascending=False).head(500),
    df[df.gram == 2].sort_values("rank",ascending=False).head(500),
    df[df.gram == 3].sort_values("rank",ascending=False).head(500),
    )).to_csv(args.output)
