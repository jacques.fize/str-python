# coding = utf-8

from shapely.geometry import Point
from ..nlp.disambiguator.share_prop import ShareProp
from ..nlp.disambiguator.most_common import MostCommonDisambiguator
from ..nlp.disambiguator.wikipedia_cooc import WikipediaDisambiguator

import langdetect
import pandas as pd
import re
import glob, re, sys

disMost_common = MostCommonDisambiguator()
disGaurav = ShareProp()
disWiki = WikipediaDisambiguator()
from ..helpers.geodict_helpers import gazetteer

def get_coord(id):
    try:
        c = gazetteer.get_by_id(id)[0].coord
        return Point(c.lon, c.lat)
    except Exception as e:
        return None


def dist(id1, id2):
    p1, p2 = get_coord(id1), get_coord(id2)
    if not p1 or not p2:
        return -1
    else:
        return p1.distance(p2)


def efficiencyMostCommon(df, lang, score="accuracy",k=1):
    df2 = df[-df["GID"].isin(["O", "NR", "o"])][["text", "GID"]]
    def foo(lang,x):
        res = disMost_common.disambiguate(lang, toponyms=[x])
        if x in res:
            return res[x]
        return "O"
    df2["disambiguation"] = df2.text.apply(lambda x:foo(lang,x))
    if score == "mean_distance_error":
        df2["distance"] = df2.apply(lambda row: dist(row.GID, row.disambiguation) if "GID" in row else -1, axis=1)
        return df2["distance"][df2["distance"] >= 0].mean()
    if score == "accuracy_k":
        df2["distance"] = df2.apply(lambda row: dist(row.GID, row.disambiguation) if "GID" in row else -1, axis=1)
        return ((df2["distance"] < k) & (df2["distance"] >= 0)).sum() / ((df2["distance"] >= 0).sum())
    return (df2.GID == df2.disambiguation).sum() / len(df2)


def efficiencyGeodict(df, lang, score="accuracy",k=1):
    df2 = df[-df["GID"].isin(["O", "NR", "o"])][["text", "GID"]]
    res_dis = disGaurav.disambiguate(lang,toponyms=df2["text"].unique().tolist())
    df2["disambiguation"] = df2.text.apply(lambda x: res_dis[x] if x in res_dis else None)
    if score == "mean_distance_error":
        df2["distance"] = df2.apply(lambda row: dist(row.GID, row.disambiguation) if "GID" in row else -1, axis=1)
        return df2["distance"][df2["distance"] >= 0].mean()
    if score == "accuracy_k":
        df2["distance"] = df2.apply(lambda row: dist(row.GID, row.disambiguation) if "GID" in row else -1, axis=1)
        return ((df2["distance"] < k) & (df2["distance"] >= 0)).sum() / ((df2["distance"] >= 0).sum())

    return (df2.GID == df2.disambiguation).sum() / len(df2)


def efficiencyWiki(df, lang, score="accuracy",k=1):
    df2 = df[-df["GID"].isin(["O", "NR", "o"])][["text", "GID"]]
    res_dis = disWiki.disambiguate(lang,toponyms=df2["text"].unique().tolist())
    df2["disambiguation"] = df2.text.apply(lambda x: res_dis[x] if x in res_dis else None)
    if score == "mean_distance_error":
        df2["distance"] = df2.apply(lambda row: dist(row.GID, row.disambiguation) if "GID" in row else -1, axis=1)
        return df2["distance"][df2["distance"] >= 0].mean()
    elif score == "accuracy_k":
        df2["distance"] = df2.apply(lambda row: dist(row.GID, row.disambiguation) if "GID" in row else -1, axis=1)
        return ((df2["distance"] < k) & (df2["distance"] >= 0)).sum() / ((df2["distance"] >= 0).sum())
    else:
        return (df2.GID == df2.disambiguation).sum() / len(df2)


def parse_file_EPI(fn, path_rawtext):
    id_ = int(re.findall(r"\d+", fn)[-1])
    lang = langdetect.detect(open("{1}/{0}.txt".format(id_, path_rawtext.rstrip("/"))).read())
    df = pd.read_json(fn, orient="index")
    try:
        df = df[(df["type"] == "location") & (df["annotation"] == "correct")]
    except:
        return
    df["text"] = df["content"].apply(lambda x: re.sub(r"\s+", " ", x.strip()))
    df["geoname"] = df["info"].apply(lambda x: x["id"])
    df["GID"] = df["geoname"].apply(lambda x: gazetteer.get_by_other_id(x,"geonames").id)
    df = df[df["geoname"] != -111111]

    return df, lang
