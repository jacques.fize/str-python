# coding = utf-8

def is_pareto_front(dataf, row, columns):
    """
    Return true if the combination of data for the columns 'columns' is a pareto optimum in 'dataf'.
    :param dataf:
    :param row:
    :param columns:
    :return:
    """
    values = [row[col] for col in columns]
    boolean_is_max = []
    for c in range(len(columns)):
        val = values[c]
        col = columns[c]
        bool_temp = True
        for c2 in range(len(columns)):
            if c != c2: break
            val2 = values[c]
            col2 = columns[c]
            bool_temp = bool_temp and (dataf.loc[dataf[col2] == val2].max()[col] <= val)
        boolean_is_max.append(bool_temp)
    # if no criteria superior
    daf = dataf.copy()
    for c in range(len(columns)):
        val = values[c]
        col = columns[c]
        daf = daf.loc[(daf[col] > val)]
    return sum(map(int, boolean_is_max)) == len(columns) and len(daf) == 0
