# coding = utf-8

from ..helpers.geodict_helpers import gazetteer
import numpy as np


def flattern(A):
    rt = []
    for i in A:
        if isinstance(i, list):
            rt.extend(flattern(i))
        elif isinstance(i, np.ndarray):
            rt.extend(flattern(i.tolist()))
        else:
            rt.append(i)
    return rt


def most_common(lst):
    if not lst:
        return "P-PPL"
    if len(list(set(lst))) > 1 and "P-PPL" in set(lst):
        lst = [x for x in lst if x != "PPL"]
    return max(set(lst), key=lst.count)


def granularity(graph):
    """
    Return the granularity of a STR
    :param graph:
    :return:
    """
    class_list = flattern([gazetteer.get_by_id(n)[0].class_ for n in list(graph.nodes())])
    if not class_list:
        return "P-PPL"
    return most_common(class_list)
