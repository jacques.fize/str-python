import numpy as np
import pandas as pd
from textblob import Blobber
from textblob_fr import PatternTagger, PatternAnalyzer

from .terminology.terminology_matcher import Matcher



def parse_match_result(match_res):
    """
    Delete unnecessary matches or double

    Exemple
     - erosion [15:16] [Will be deleted]
     - erosion hydrique [15:17] 
    """
    prevstart = None
    res_final = []
    for i, item in enumerate(match_res):
        if prevstart:
            if item[1] == prevstart:
                del res_final[-1]
        prevstart = item[1]
        res_final.append(item)
    return res_final


def diff(x, other_windows, window_size=5):
    """
    Return the difference from the position of a token with other tokens.
    """
    mask_1 = (np.abs(x.start_token - other_windows.end_token) < window_size)
    mask_2 = (np.abs(other_windows.start_token - x.end_token) < window_size)
    mask_3 = (mask_1 | mask_2)
    res_ = other_windows[mask_3].hash_id.values.tolist()
    if not res_:
        return np.nan
    return res_


def is_in(x, other_windows):
    mask_1 = (other_windows.start_token > x.start)
    mask_2 = (other_windows.start_token < x.end)
    mask_3 = (mask_1 & mask_2)
    res_ = other_windows[mask_3].hash_id.values.tolist()
    if not res_:
        return np.nan
    return res_


class ThematicSpatialEntitiesMatcher:
    """
    This class is used to return the spatial entities and terms found in a common area(sentence,window) in a tex


    """

    def __init__(self, terminology_matcher,lang):
        """
        ThematicSpatialEntitiesMatcher constructor

        Parameters
        ----------
        terminology_matcher : terminology_matcher.TerminologyMatcher
            thelmatic terminology matcher
        lang_model : spacy.lang.Language
            Spacy language model 

        """
        self.lang=lang
        self.terminology_ = terminology_matcher
        self.matcher_spatial_entities = Matcher(lang=lang,use_lemma=False,use_lower=False,use_plural=False,use_singular=False)
        if lang == "fr": 
            self.blobber = Blobber(pos_tagger=PatternTagger(), analyzer=PatternAnalyzer())
        else:
            self.blobber= Blobber()

    def update_se_matcher(self, str_object):
        """
        Reinitialise the spatial entity matcher to detect the entities of the str

        Parameters
        ----------
        str_object : strpython.models.STR
            str instance

        """
        self.matcher_spatial_entities = Matcher(lang=self.lang,use_lemma=False,use_lower=False,use_plural=False,use_singular=False)
        for entity in str_object.spatial_entities.values():
            self.matcher_spatial_entities.add(entity, None, entity.split())

    def get_thematic_matches(self, doc):
        """
        Return the position of the term matched in the text

        Parameters
        ----------
        doc : spacy.tokens.doc.Doc
            Spacy Object

        Returns
        -------
        tuple
            (word_id, start, end)
        """
        return parse_match_result([list(d) for d in self.terminology_(doc)])

    def get_spatial_entities_matches(self, doc, str_object):
        """
        Return the position of the spatial entities matched in the text

        Parameters
        ----------
        doc : spacy.tokens.doc.Doc
            Spacy Object
        str_object : strpython.models.STR
            str instance

        Returns
        -------
        tuple
            (word_id, start, end)
        """
        self.update_se_matcher(str_object)
        return parse_match_result([list(d) for d in self.matcher_spatial_entities(doc)])

    def get_matches(self, doc, str_object):
        """
        Return both term and spatial entities matched position in two pandas.DataFrame.

        Parameters
        ----------
        doc : spacy.tokens.doc.Doc  
            Spacy representation of the text
        str_object : strpython.models.STR
            str instance

        Returns
        -------
        tuple
            (thematic_match,spatial_entities_match)
        """
        data_them = pd.DataFrame(self.get_thematic_matches(
            doc), columns='hash_id start_token end_token'.split())
        data_spat = pd.DataFrame(self.get_spatial_entities_matches(
            doc, str_object), columns='hash_id start_token end_token'.split())
        return data_them, data_spat

    def within_window(self, doc, str_object, window_size=5):
        """
        Return a pandas.Dataframe that contains couples of term and spatial entities in a window of certain size.

        Parameters
        ----------
        doc : spacy.tokens.doc.Doc  
            Spacy representation of the text
        str_object : strpython.models.STR
            str instance
        window_size : int, optional
            size of the window (the default is 10)

        Returns
        -------
        pandas.Dataframe
            contains word ids of spatial entities and term matched
        """
        data_them, data_spat = self.get_matches(doc, str_object)
        data_them["close_spat"] = data_them["start_token end_token".split()].apply(lambda x: diff(x, data_spat), axis=1)
        data_them["spat_in"] = data_them["close_spat"]
        data_them["theme_in"] = data_them["hash_id"].apply(lambda x: [x])
        data_them = data_them[~pd.isna(data_them["theme_in"])]
        data_them = data_them[~pd.isna(data_them["spat_in"])]

        return data_them

    def within_sentence(self, doc, str_object):
        """
        Return a pandas.Dataframe that contains couples of term and spatial entities found in the same sentence.

        Parameters
        ----------
        doc : spacy.tokens.doc.Doc  
            Spacy representation of the text
        str_object : strpython.models.STR
            str instance

        Returns
        -------
        pandas.Dataframe
            contains word ids of spatial entities and term matched
        """
        data_them, data_spat = self.get_matches(doc, str_object)
        pp = []
        for i, sent in enumerate(self.blobber(doc).sentences):
            pp.append([i, sent.start, sent.end])
        data_sent = pd.DataFrame(pp, columns="id_sent start end".split())
        data_sent["theme_in"] = data_sent.apply(
            lambda x: is_in(x, data_them), axis=1)
        data_sent["spat_in"] = data_sent.apply(
            lambda x: is_in(x, data_spat), axis=1)

        data_sent = data_sent[~pd.isna(data_sent["theme_in"])]
        data_sent = data_sent[~pd.isna(data_sent["spat_in"])]
        return data_sent

    def within_paragraph(self, doc, str_object):
        """
        Return a pandas.Dataframe that contains couples of term and spatial entities in the same paragraph.

        Parameters
        ----------
        doc : spacy.tokens.doc.Doc  
            Spacy representation of the text
        str_object : strpython.models.STR
            str instance
        Returns
        -------
        pandas.Dataframe
            contains words ids of spatial entities and term matched
        """
        data_them, data_spat = self.get_matches(doc, str_object)

        matcher_para = Matcher(lang=self.lang,use_lemma=False,use_lower=False,use_plural=False,use_singular=False)
        matcher_para.add("PARA", None, ["-PARA-"])
        pp = []
        for para in matcher_para(doc):
            if not pp:
                start = para[1]
            pp.append([0, start, para[1]])
            start = para[1]

        data_para = pd.DataFrame(pp[1:], columns="id_para start end".split())
        data_para["theme_in"] = data_para.apply(
            lambda x: is_in(x, data_them), axis=1)
        data_para["spat_in"] = data_para.apply(
            lambda x: is_in(x, data_spat), axis=1)

        data_para = data_para[~pd.isna(data_para["theme_in"])]
        data_para = data_para[~pd.isna(data_para["spat_in"])]

        return data_para
