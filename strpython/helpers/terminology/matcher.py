import pandas as pd
import numpy as np
import re, json, os

from mytoolbox.exception.inline import safe_execute
from mytoolbox.text.clean import resolv_a

from .terminology_matcher import TerminologyMatcher

package_directory = os.path.dirname(os.path.abspath(__file__+"../../../"))

def matcher_inra_voc(lang):
    """
    Return a terminolgy matcher using the INRA vocabulary constructed on INRA agroecology dictionnary.
    
    Parameters
    ----------
    nlp : spacy.lang.Language
        model
    lang : str
        language of the terms
    fn : str, optional
        filename (the default is "data/target_theme_words.{0}.txt")
    
    Returns
    -------
    TerminologyMatcher
        matcher
    """
    return TerminologyMatcher(open(os.path.join(package_directory,"resources/terminology/inra/target_theme_words.{0}.txt".format(lang))).read().strip().split("\n"))


def matcher_devdura_voc( lang):
    """
    Return a terminology matcher using the Sustainable Developement Vocabulary from the French Administration (Available here: http://www.culture.gouv.fr/Thematiques/Langue-francaise-et-langues-de-France/Politiques-de-la-langue/Enrichissement-de-la-langue-francaise/FranceTerme/Vocabulaire-du-developpement-durable-2015)
    
    Parameters
    ----------
    nlp : spacy.lang.Language
        spacy model
    fn : str, optional
        filename (the default is "data/Vocabulaire_du_Developement_Durable.csv")
    
    Returns
    -------
    TerminologyMatcher
        matcher
    """
    if lang == "fr":
        terminology = pd.read_csv(os.path.join(package_directory, "resources/terminology/dev_durable/Vocabulaire_du_Developement_Durable.csv"),sep=";")["label"].values.tolist()
    else:
        old_terminology = pd.read_csv(os.path.join(package_directory,"resources/terminology/dev_durable/Vocabulaire_du_Developement_Durable.csv"),sep=";")["alt_labels"].apply(eval)
        terminology = []
        [terminology.extend(t) for t in old_terminology]

    terminology = [re.sub("([|[\(]).*(]|[\)])?","",x).strip() for x  in terminology]

    return TerminologyMatcher( terminology)


def matcher_lda_voc(lang):
    """
    Return a terminology matcher using the terminolgy built from the LDA Topic's top words.
    
    Parameters
    ----------
    nlp : spacy.lang.Language
        spacy model
    
    Returns
    -------
    TerminologyMatcher
        matcher
    """
    terminology = pd.read_csv(os.path.join(package_directory,"resources/terminology/lda_simple/topic2.csv"))["Term"].values.tolist()
    terminology.extend(pd.read_csv(os.path.join(package_directory,"resources/terminology/lda_simple/topic4.csv"))["Term"].values.tolist())
    return TerminologyMatcher(terminology)


def matcher_agrovoc( lang):
    """
    Return a terminolgy matcher using the Agrovoc vocabulary.
    
    Parameters
    ----------
    nlp : spacy.lang.Language
        model
    lang : str
        language of the terms
    
    Returns
    -------
    TerminologyMatcher
        matcher
    """
    agrovoc_vocab = pd.read_csv(os.path.join(package_directory,"resources/terminology/agrovoc/agrovoc_cleaned2.csv"))
    #indexes_to_ignore = json.load(open(os.path.join(package_directory,"resources/terminology/agrovoc/entry_to_ignore.json")))

    agrovoc_vocab["preferred_label_new"] = agrovoc_vocab["preferred_label_new"].apply(
        lambda x: safe_execute({}, Exception, json.loads, x.replace("\'", "\"")))
    agrovoc_vocab["label_lang"] = agrovoc_vocab["preferred_label_new"].apply(
        lambda x: str(resolv_a(x[lang]) if lang in x else np.nan).strip().lower())
    agrovoc_vocab=agrovoc_vocab[~pd.isna(agrovoc_vocab["label_lang"])]

    #agrovoc_vocab["syns"] = agrovoc_vocab.Synonyms.apply(lambda x : [] if pd.isna(x) else x.split("|"))
    terminology = np.array(agrovoc_vocab["label_lang"].values.tolist())
    return TerminologyMatcher(terminology.tolist())

def matcher_biotex(lang):
    """
    Return a terminolgy matcher using the INRA vocabulary constructed on Biotex output.
    
    Parameters
    ----------
    nlp : spacy.lang.Language
        model
    
    Returns
    -------
    TerminologyMatcher
        matcher
    """
    df=pd.read_csv(os.path.join(package_directory,"resources/terminology/biotex/biotex_bvlac_1500.csv"),index_col=0)
    return TerminologyMatcher(df.term.values.tolist())

def matcher_biotex_lda(lang):
    """
    Return a terminolgy matcher using the INRA vocabulary constructed on LDA output coconstructed with BIotex ouptut.
    
    Parameters
    ----------
    nlp : spacy.lang.Language
        model
    
    Returns
    -------
    TerminologyMatcher
        matcher
    """
    df=pd.read_csv(os.path.join(package_directory,"resources/terminology/mixed/lda_top1000_4topic_1500.csv"),index_col=0)
    return TerminologyMatcher( df.term.values.tolist())



def matcher_biotex_padi_500(lang):
    """
    Return a terminolgy matcher using the Biotex output on the PADIWEB golden standard.
    
    Parameters
    ----------
    nlp : spacy.lang.Language
        model
    
    Returns
    -------
    TerminologyMatcher
        matcher
    """
    df=pd.read_csv(os.path.join(package_directory,"resources/terminology/biotex/biotex_padi500_1500.csv"),index_col=0)
    df["char_n"]=df.label.apply(lambda x:len(str(x)))
    df["gram"]=df.label.apply(lambda x: len(str(x).split()))
    return TerminologyMatcher(df.label.values.tolist())

def matcher_biotex_padi_35k(lang):
    """
    Return a terminolgy matcher using the Biotex output on the PADIWEB 35k.
    
    Parameters
    ----------
    nlp : spacy.lang.Language
        model
    
    Returns
    -------
    TerminologyMatcher
        matcher
    """
    df=pd.read_csv(os.path.join(package_directory,"resources/terminology/biotex/biotex_padi_35k_1500.csv"),index_col=0)
    df["char_n"]=df.term.apply(lambda x:len(str(x)))
    return TerminologyMatcher(df.term.values.tolist())



def matcher_padi_agrovoc_wiki_umls(lang):
    """
    Return a terminolgy matcher using the vocabulary built by Sarah on the PADIWEB corpus.
    
    Parameters
    ----------
    nlp : spacy.lang.Language
        model
    
    Returns
    -------
    TerminologyMatcher
        matcher
    """
    def extract_alias(x,lang):
        aliases=[]
        for item in x:
            if lang == item["lang"]:
                aliases.append(item["label"])
        return aliases

    df=pd.read_pickle(os.path.join(package_directory,"resources/terminology/mixed/agrovoc_umls_wiki_disease_padiweb.pkl"))
    df.prefLabel=df.prefLabel.astype(str)
    df["wiki_alias_{0}".format(lang)]=df.other.apply(lambda x: extract_alias(x["wiki"]["alias"],lang)if "wiki" in x else[])
    df["agrovoc_alias_{0}".format(lang)]=df.other.apply(lambda x: extract_alias(x["agrovoc"]["alias"],lang))
    df["umls_alias_{0}".format(lang)]=df.other.apply(lambda x: extract_alias(x["umls"]["alias"],lang)if "umls" in x else[])
    df["alias_{0}".format(lang)]=df["umls_alias_{0} agrovoc_alias_{1} wiki_alias_{2}".format(lang,lang,lang).split()].apply(lambda x: np.hstack(np.array(x.values)),axis=1)
    df["alias_{0}".format(lang)]=df["alias_{0}".format(lang)].apply(lambda x: [str(i) for i in x])
    return TerminologyMatcher(df,lang=lang,column_id=None,column_label="prefLabel",column_alt_label="alias_{0}".format(lang))


def matcher_prenom_en_fr(lang):
    """
    Return a terminolgy matcher using list of firstname used in english and in french.
    
    Parameters
    ----------
    nlp : spacy.lang.Language
        model
    
    Returns
    -------
    TerminologyMatcher
        matcher
    """
    df=pd.read_csv(os.path.join(package_directory,"resources/terminology/prenom/prenom_corrected_filtered.csv"),index_col=0)
    return TerminologyMatcher(df["01_prenom"].values.tolist())



def stop_word_bvlac(lang):
    """
    Return a terminolgy matcher using list of firstname used in english and in french.
    
    Parameters
    ----------
    nlp : spacy.lang.Language
        model
    
    Returns
    -------
    TerminologyMatcher
        matcher
    """
    df=pd.read_csv(os.path.join(package_directory,"resources/terminology/mixed/stop_spec_bvlac.csv"),index_col=0)
    return TerminologyMatcher(df.term.values)

list_matchers={
    "inra":matcher_inra_voc,
    "dev_du":matcher_devdura_voc,
    "lda":matcher_lda_voc,
    "agrovoc":matcher_agrovoc,
    "biotex_bvlac":matcher_biotex,
    "biotex_lda_bvlac":matcher_biotex_lda,
    "biotex_padi_500":matcher_biotex_padi_500,
    "biotex_padi_35k":matcher_biotex_padi_35k,
    "sarah_vocab":matcher_padi_agrovoc_wiki_umls,
    "prenom_en_fr" : matcher_prenom_en_fr,
    "stop_word_bvlac":stop_word_bvlac
    }