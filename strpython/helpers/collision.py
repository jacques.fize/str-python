import json
import os
import warnings

from shapely.geometry import  Point

from ..config.configuration import config
from .geodict_helpers import gazetteer
import geopandas as gpd

__cache = {}
__cache_adjacency = {}
__limit_cache = 10000
__cache_frequency = {}


def add_cache(id_, hull):
    """
    Add the extracted data to a cache instance. This process manage the cache based on a limit defined in `__limit_cache`.
    If this limit is attained, the less used data deleted from the cache.
    :param id_: id of the first spatial entity
    :param hull: data
    :return:
    """
    global __cache, __limit_cache, __cache_frequency
    __cache[id_] = hull
    if not id_ in __cache_frequency: __cache_frequency[id_] = 0
    __cache_frequency[id_] += 1


def add_cache_adjacency(id_se1, id_se2):
    """
    Add the adjacency between two spatial entitiy in a cache variable.
    :param id_se1: id of the first spatial entity
    :param id_se2: id of the second spatial entity
    :return:
    """
    global __cache_adjacency
    if not id_se1 in __cache_adjacency:
        __cache_adjacency[id_se1] = {}
    __cache_adjacency[id_se1][id_se2] = True


def explode(gdf):
    """
    Explodes a geodataframe

    Will explode muti-part geometries into single geometries. Original index is
    stored in column level_0 and zero-based count of geometries per multi-
    geometry is stored in level_1

    Args:
        gdf (gpd.GeoDataFrame) : input geodataframe with multi-geometries

    Returns:
        gdf (gpd.GeoDataFrame) : exploded geodataframe with a new index
                                 and two new columns: level_0 and level_1

    """
    gs = gdf.explode()
    gdf2 = gs.reset_index().rename(columns={0: 'geometry'})
    gdf_out = gdf2.merge(gdf.drop('geometry', axis=1), left_on='level_0', right_index=True)
    gdf_out = gdf_out.set_index(['level_0', 'level_1']).set_geometry('geometry')
    gdf_out.crs = gdf.crs
    return gdf_out.reset_index(level=[0, 1])


def getGEO(id_se):
    """
    Get the geofootprint of a spatial entity. If found, this geofootprint is a shape extracted from OSM. If not,
    coordinates are used.
    :param id_se: id of the spatial entity
    :return: geopandas.GeoSeries
    """
    data = gazetteer.get_by_id(id_se)
    if not data:
        return None

    data=data[0]
    if "path" in data.other:
        return gpd.read_file(os.path.join(config.osm_boundaries_directory, data.other["path"])).convex_hull
    elif "coord" in data.other:
        return gpd.GeoDataFrame(gpd.GeoSeries([Point(data.coord.lon, data.coord.lat).buffer(0.5)])).rename(
            columns={0: 'geometry'})
    return None



def collide(se1, se2):
    """
    Return true, if two entities convex hull intersects.
    :param se1: id of the first spatial entity
    :param se2: id of the second spatial entity
    :return:
    """
    try:
        if se1 in __cache:
            data_se1 = __cache[se1]
            __cache_frequency[se1] += 1
        else:
            data_se1 = getGEO(se1)
            add_cache(se1, data_se1)
        if se2 in __cache:
            data_se2 = __cache[se2]
            __cache_frequency[se2] += 1
        else:
            data_se2 = getGEO(se2)
            add_cache(se2, data_se2)
    except Exception as e:
        warnings.warn(e)
        return False
    if not type(data_se1) == gpd.GeoDataFrame or not type(data_se2) == gpd.GeoDataFrame:
        return False
    try:
        if data_se1.envelope.intersects(data_se2.envelope):
            if data_se1.intersects(data_se2):
                return True
    except:
        if data_se1.envelope.intersects(data_se2.envelope).any():
            if data_se1.intersects(data_se2).any():
                return True
    return False


def collisionTwoSEBoundaries(id_se1, id_se2):
    """
    Return True if two spatial entities are adjacent.
    :param id_se1: id of the first spatial entity
    :param id_se2: id of the second spatial entity
    :return:
    """
    global __cache, __cache_adjacency
    if id_se1 in __cache_adjacency:
        if id_se2 in __cache_adjacency[id_se1]:
            return __cache_adjacency[id_se1][id_se2]
    elif id_se2 in __cache_adjacency:
        if id_se1 in __cache_adjacency[id_se2]:
            return __cache_adjacency[id_se2][id_se1]

    if not id_se1 in __cache_adjacency:
        __cache_adjacency[id_se1] = {}

    if collide(id_se1, id_se2):  # and not include_in(h1,h2):
        __cache_adjacency[id_se1][id_se2] = True
        return True
    __cache_adjacency[id_se1][id_se2] = False
    return False

