# coding = utf-8
import sqlite3
import os

from ..config.configuration import config

class GeoRelationMatchingDatabase():
    def __init__(self, db_filename = config.relation_db_path):
        if os.path.exists(db_filename):
            try:
                self._db_connection = sqlite3.connect(db_filename)
            except:
                raise ValueError("File is not a sqlite database")
        else:
            self._db_connection = sqlite3.connect(db_filename)
            self.__init_database__()

    def __init_database__(self):
        """
        Initialize database
        """
        cursor = self._db_connection.cursor()
        inclusion_schema = """CREATE TABLE inclusion
        (idse1 text, idse2 text, value integer)
        """
        adjacency_schema = """CREATE TABLE adjacency
        (idse1 text, idse2 text, value integer)
        """
        matching_schema = """CREATE TABLE matching
        (dataset text, g1 integer, g2 integer, c1 integer, c2 integer, c3 integer,c4 integer, c5 REAL, c6 REAL )
        """
        cursor.execute(inclusion_schema)
        cursor.execute(adjacency_schema)
        cursor.execute(matching_schema)

        self._db_connection.commit()
        cursor.close()

    def add_adjacency(self, idse1: str, idse2: str, value: bool):
        """
        Add adjacency relation value

        Parameters
        ----------
        idse1 : str
            spatial entity id1
        idse2 : str
            spatial entity id2
        value : bool
            value of the relation

        """

        cursor = self._db_connection.cursor()
        cursor.execute('INSERT INTO adjacency VALUES(?,?,?)', (idse1, idse2, int(value)))
        self._db_connection.commit()
        cursor.close()

    def add_inclusion(self, idse1: str, idse2: str, value: bool):
        """
        Add inclusion relation in the database
        Parameters
        ----------
        idse1 : str
            id of the first spatial entity
        idse2 : str
            id of the second spatial entity
        value : bool
            value of the relation

        """
        cursor = self._db_connection.cursor()
        cursor.execute('INSERT INTO inclusion VALUES(?,?,?)', (idse1, idse2, int(value)))
        self._db_connection.commit()
        cursor.close()

    def add_matching(self, dataset: str, G1: int, G2: int, c1: bool, c2: bool, c3: bool, c4: bool,c5: float, c6: float):
        """
        Add a matching criteria result within the database
        Parameters
        ----------
        dataset : str
            name of the dataset from where the matching have been done
        G1 : int
            id of the first STR
        G2 : int
            id of the second STR
        c1 : bool
            value of criterion 1
        c2 : bool
            value of criterion 2
        c3 : bool
            value of criterion 3
        c4 : bool
            value of criterion 4
        c5 : float
            value of criterion 5
        c6 : float
            value of criterion 6

        """
        cursor = self._db_connection.cursor()
        cursor.execute('INSERT INTO matching VALUES(?,?,?,?,?,?,?,?,?)',
                       (dataset, G1, G2, int(c1), int(c2), int(c3), int(c4),float(c5),float(c6)))
        self._db_connection.commit()
        cursor.close()

    def get_spatial_relation(self, idse1: str, idse2: str, table: str):
        """
        Return the value of the spatial relation if exist in the table
        Parameters
        ----------
        idse1 : str
            id of the first spatial entity
        idse2 : str
            id of the first spatial entity
        table : str
            name of table that store the relation value desired

        Returns
        -------
        bool,bool
            (True if relation found,value of the relation)
        """
        cursor = self._db_connection.cursor()
        cursor.execute(
            "SELECT * from {2} a where a.idse1 LIKE '{0}' and a.idse2 LIKE '{1}'".format(idse1, idse2, table))
        result_ = cursor.fetchone()
        if not result_ and table != "inclusion":
            cursor.execute(
                "SELECT * from {2} a where a.idse2 LIKE '{0}' and a.idse1 LIKE '{1}'".format(idse1, idse2, table))
            result_ = cursor.fetchone()
        cursor.close()
        if result_:
            return True, bool(result_[-1])
        return False, False

    def get_adjacency(self, idse1: str, idse2: str):
        """

        Parameters
        ----------
        idse1 : str
            id of the first spatial entity
        idse2 : str
            id of the first spatial entity

        Returns
        -------

        """
        return self.get_spatial_relation(idse1, idse2, "adjacency")

    def get_inclusion(self, idse1: str, idse2: str):
        """

        Parameters
        ----------
        idse1 : str
            id of the first spatial entity
        idse2 : str
            id of the first spatial entity

        Returns
        -------

        """
        return self.get_spatial_relation(idse1, idse2, "inclusion")

    def get_matching(self, G1: int, G2: int, dataset: str):
        cursor = self._db_connection.cursor()
        cursor.execute("SELECT * from {2} a where a.dataset LIKE '{3}' AND a.g1 = {0} and a.g2 = {1}".format(G1, G2, "matching",dataset))
        result_ = cursor.fetchone()
        cursor.close()
        if result_:
            return True, tuple(map(float, result_[-6:]))
        return False, False


if __name__ == "__main__":
    if os.path.exists("test2.db"):
        os.remove("test2.db")
    g = GeoRelationMatchingDatabase("test2.db")
    g.add_adjacency("GD1", "GD2", True)
    assert g.get_adjacency("GD1", "GD2") == (True, True)
    assert g.get_adjacency("GD2", "GD1") == (True, True)

    g.add_inclusion("GD1", "GD2", True)
    assert g.get_inclusion("GD1", "GD2") == (True, True)
    assert g.get_inclusion("GD2", "GD1") == (False, False)

    g.add_matching("test", 1, 2, True, True, False, True,0.,0.)
    g.add_matching("test2", 1, 2, True, False, False, True,0.,0.)
    assert g.get_matching(1, 2, "test") == (True, (True, True, False, True,0.,0.))
    assert g.get_matching(1, 2, "test2") != (True, (True, True, False, True,0.,0.))
    print("Passed the tests !")

