# coding = utf-8
from ..helpers.geodict_helpers import gazetteer


class RelationCache():
    __cache_entity_data = {}

    def __init__(self, pre_computed={},two_ways=False):
        self.db_rel_match = pre_computed
        self.two_way = two_ways

    def entity_exists(self, es ):
        return es in self.db_rel_match

    def update_from_dict(self, new_relations: dict):
        for es in new_relations:
            if not es in self.db_rel_match: self.db_rel_match[es] = {}
            for es2 in new_relations[es]:
                if not es2 in self.db_rel_match[es]:
                    self.db_rel_match[es][es2] = new_relations[es][es2]
                    if self.two_way:
                        self.db_rel_match[es2][es] = new_relations[es][es2]

    def is_relation(self, id_se1: str, id_se2: str):
        raise NotImplementedError()

    def get_data(self, id_se):
        """
        Return an gazpy.Element object containing information about a spatial entity.

        Parameters
        ----------
        id_se : str
            Identifier of the spatial entity

        Returns
        -------
        gazpy.Element
            data
        """

        if id_se in RelationCache.__cache_entity_data:
            return RelationCache.__cache_entity_data[id_se]
        data = gazetteer.get_by_id(id_se)
        if len(data) > 0:
            RelationCache.__cache_entity_data[id_se] = data[0]
            return data[0]

    def in_cache(self, id_se1: str, id_se2: str):
        raise NotImplementedError()

    def add_cache(self,id_se1: str, id_se2: str, value : bool):
        if id_se1 not in self.db_rel_match:
            self.db_rel_match[id_se1] = {}
        if self.two_way and id_se2 not in self.db_rel_match:
            self.db_rel_match[id_se2] = {}

        self.db_rel_match[id_se1][id_se2] = value
        if self.two_way:
            self.db_rel_match[id_se2][id_se1] = value


class InclusionRelation(RelationCache):

    def __init__(self, precomputed={}):
        RelationCache.__init__(self, precomputed)

    def in_cache(self, id_se1: str, id_se2: str):
        if id_se1 in self.db_rel_match and id_se2 in self.db_rel_match[id_se1]:
            return True, self.db_rel_match[id_se1][id_se2]
        return False, False


    def is_relation(self, id_se1: str, id_se2: str):
        found_, value = self.in_cache(id_se1, id_se2)
        if found_:
            return value



class AdjacencyRelation(RelationCache):

    def __init__(self, precomputed={}):
        RelationCache.__init__(self, precomputed)

    def in_cache(self, id_se1: str, id_se2: str):
        if id_se1 in self.db_rel_match and id_se2 in self.db_rel_match[id_se1]:
            return True, self.db_rel_match[id_se1][id_se2]
        elif id_se2 in self.db_rel_match and id_se1 in self.db_rel_match[id_se2]:
            return True, self.db_rel_match[id_se2][id_se1]
        return False, False

    def is_relation(self, id_se1: str, id_se2: str):
        found_, value = self.in_cache(id_se1, id_se2)
        if found_:
            return value


class SpatialRelationsCache():

    def __init__(self,precomputed_adj,precomputed_inc):
        self.inclusion = InclusionRelation(precomputed_inc)
        self.adjacency = AdjacencyRelation(precomputed_adj)

    def update(self,precomputed_adj,precomputed_inc):
        self.inclusion.update_from_dict(precomputed_inc)
        self.adjacency.update_from_dict(precomputed_adj)


relation_cache = SpatialRelationsCache({},{})