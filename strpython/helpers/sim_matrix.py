# coding = utf-8

import argparse, bz2, os
import json

import pandas as pd
import numpy as np


def read_bz2_matrix(file_path):
    f = bz2.BZ2File(file_path, 'r')
    matrix_ = np.load(f)
    return matrix_


def filter_selected(matrix, selected):
    return matrix[[selected]]


def read_and_load(file_path, selected=None, bz2=True):
    matrix = None
    if bz2:
        matrix = read_bz2_matrix(file_path)
    else:
        matrix = np.load(file_path)
    if selected:
        return filter_selected(matrix, selected)
    else:
        return matrix


def matrix_to_pandas_dataframe(matrix, selected, sim_measure, type_str, n=5):
    sim, type_ = sim_measure, type_str
    tab_array = []
    for line in selected:
        top_n = np.argsort(matrix[line])[::-1][1:n + 1]
        rank = 1
        for val in top_n:
            tab_array.append([line, val, sim, type_, rank, 0, 0, 0, 0,300000,0])
            rank += 1
    return pd.DataFrame(tab_array, columns="G1 G2 sim_measure type_str rank c1 c2 c3 c4 c5 c6".split())
