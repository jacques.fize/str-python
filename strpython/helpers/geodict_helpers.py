# coding=utf-8
import math
import re

from elasticsearch import Elasticsearch
from ..config.configuration import config
import pandas as pd


import gazpy as ga

gazetteer = ga.Geodict(es_client=Elasticsearch(config.es_server)) if config.gazetteer == "geodict" else ga.Geonames(
    Elasticsearch(config.es_server))


def get_most_common_id_v3(label, lang='fr'):
    """
    Return the spatial entity and its score, based on a specific label and language that obtains the highest score.
    The difference with the V2 is that it takes special cases:
     * english placenames in a french text
     * alias like China which designated also a spatial entity
    :param label:
    :param lang:
    :return:
    """
    label = label.strip()
    id_,score=None,-1
    data = gazetteer.get_by_label(label, lang)
    if data:
        id_, score = data[0].id,data[0].score
        data2=gazetteer.get_by_alias(label, lang)
        if data2 and data2[0].score > data[0].score:
            data2=data2[0]
            id_, score = data2.id, data2.score
        # simi = gazetteer.get_n_label_similar(label, lang, n=5)
        # if simi:
        #     id_3, score3 = simi[0].id, simi[0].score
        #     if id_3 and score3 > score:
        #         id_, score = id_3, score3

        return gazetteer.get_by_id(id_)[0]

    # if nothing found in english, search in aliases
    data = gazetteer.get_by_alias(label, lang)
    if data:
        return data[0] #data[0].id, data[0].score

    # similar_label = gazetteer.get_n_label_similar(label, lang, n=5)
    # if similar_label:
    #     return similar_label[0]#similar_label[0].id, similar_label[0].score

    # similar_alias = gazetteer.get_n_alias_similar(label, lang, n=5)
    # if similar_alias:
    #     return similar_alias[0]#similar_alias[0].id, similar_alias[0].score

    return None


def get_top_candidate(label, lang, n=5):
    """
    Return the 5-top candidates for a designated label in a specific language.
    :param label: str
    :param lang: str
    :return: list
    """
    if n < 4:
        n = 4
    res=gazetteer.get_by_label(label,lang,size=n-3,score=False)
    try:
        res.extend(gazetteer.get_n_label_similar(label,lang,n=1))
    except :
        pass
    
    try:
        res.extend(gazetteer.get_n_alias_similar(label, lang, n=1))
    except:
        pass
    res.append(get_most_common_id_v3(label, lang))

    return res
