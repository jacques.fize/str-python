# coding = utf-8

from .geo_relation_database import GeoRelationMatchingDatabase


class MatchingCache:

    def __init__(self, dataset, geo_rel_match_database=GeoRelationMatchingDatabase()):
        self.db_rel_match = geo_rel_match_database
        self.dataset = dataset

    def is_match(self, id_str1: int, id_str2: int):
        return self.db_rel_match.get_matching(id_str1, id_str2, self.dataset)

    def add(self, id_str1: int, id_str2: int, c1: int, c2: int, c3: int, c4: int, c5: float, c6: float):
        if not self.is_match(id_str1, id_str2)[0]:
            self.db_rel_match.add_matching(self.dataset, id_str1, id_str2, c1, c2, c3, c4, c5, c6)
