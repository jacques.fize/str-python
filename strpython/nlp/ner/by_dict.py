# coding = utf-8
import numpy as np
from polyglot.text import Text as PolyText

from .ner import NER
from ...helpers.geodict_helpers import gazetteer,get_most_common_id_v3


class ByDict(NER):

    def __init__(self, lang="fr", threshold=1):
        NER.__init__(self, lang)
        self.poly_instance = None
        self.threshold = threshold

    def identify(self, text):
        if isinstance(text, str):
            try:
                pos_t = PolyText(text).pos_tags
            except:
                pos_t = PolyText(text,
                                 hint_language_code=self._lang).pos_tags  # Error due to UTF8 invalid character pycld2
                pos_t = np.array(pos_t)
        else:
            pos_t = np.array(text)
        mask = np.argwhere(
            (pos_t[:, 1] == 'PROPN') | (pos_t[:, 1] == 'ADP') | (pos_t[:, 1] == 'DET') | (pos_t[:, 1] == 'ADJ'))

        # get terms
        cur = []
        terms = []
        for f, s in zip(mask, mask[1:]):
            if abs(f[0] - s[0]) == 1:
                cur.append(s[0])
            else:
                terms.append(cur)
                cur = f.tolist()

        for t in terms:
            GID = get_most_common_id_v3(" ".join(pos_t[:, 0][t]), lang=self._lang)[0]
            if GID:
                data = gazetteer.get_by_id(GID)[0]
                if "score" in data:
                    if not float(data["score"]) > self.threshold:
                        continue
                    if len(t) == 1:
                        pos_t[:, 1][t] = NER._unified_tag["place"]
                    elif len(t) == 2:
                        pos_t[:, 1][t[0]] = "BEG-" + NER._unified_tag["place"]
                        pos_t[:, 1][t[-1]] = "END" + NER._unified_tag["place"]
                    else:
                        pos_t[:, 1][t[0]] = "BEG-" + NER._unified_tag["place"]
                        pos_t[:, 1][t[1:-1]] = NER._unified_tag["place"]
                        pos_t[:, 1][t[-1]] = "END" + NER._unified_tag["place"]
        return pos_t
