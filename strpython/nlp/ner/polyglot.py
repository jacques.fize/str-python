#coding = utf-8

import json

import numpy as np
from polyglot.text import Text

from .ner import NER


class Polyglot(NER):

    def __init__(self,lang="fr"):
        NER.__init__(self,lang)
        self.poly_instance=None

    def identify(self,text):
        self.poly_instance = Text(text,hint_language_code=self._lang)
        result_=[]
        for en in self.poly_instance.entities:
            result_.append([eval(en.__str__()),self.translate_tag(en.tag)])
        return np.array(result_)

    def translate_tag(self,tag):
        if tag == "I-PER":
            return NER._unified_tag["pers"]
        if tag =="I-LOC":
            return NER._unified_tag["place"]
        if tag =="I-ORG":
            return NER._unified_tag["org"]

if __name__ == "__main__":
    t = Polyglot()
    print(t.identify(
        "J'aime les ailes de poulet du Kentucky! Elles ont un petit goût de mercure bien spécifique des États-Unis ! Donald Trump est vraiment cool d'autoriser ce genre d'entreprise à pratiquer"))
