# coding = utf-8
# coding=utf-8

from flair.data import Sentence
from flair.models import SequenceTagger

import numpy as np
from .ner import NER
from ..exception.language import LanguageNotAvailable

_flair_available_language = ["fr", "en","es","de"]

_tag_flair = {
    "place": [ "LOC"],  # Petite particularité
    "pers": "PER",
    "org": "ORG"
}

all_tags=["LOC","PER","ORG"]


class Flair(NER):
    """
    Python wrapper for StanfordNER
    """

    def __init__(self, lang="fr"):
        NER.__init__(self, lang)

        if not self._lang in _flair_available_language:
            raise LanguageNotAvailable(self._lang, self)

        self._ner = SequenceTagger.load("{0}-ner".format(self._lang))

    def split_text(self,text,maxlen=50000):
        texts=text.split(".")
        phrases_given=[]
        c=0
        current_phrase=""
        for t in texts:
            if c + len(t)+1 <maxlen:
                current_phrase+="."+t
                c+=len(t)+1
            elif c + len(t) > maxlen:
                phrases_given.append(current_phrase)
                current_phrase, c ="",0
        if not phrases_given:
            phrases_given=[text]
        return phrases_given

    def identify(self, text=None):
        import multiprocessing
        if len(text) > 10000:
            output_=[]
            for t in self.split_text(text,10000):
                sentence = Sentence(t)
                self._ner.predict(sentence)
                output_.extend([[e.text, self.translate_tag(e.tag)] for e in sentence.get_spans('ner')])

        else:
            sentence = Sentence(text)
            self._ner.predict(sentence)
            output_=[[e.text, self.translate_tag(e.tag)] for e in sentence.get_spans('ner')]
        return np.array(output_)

    def translate_tag(self, tag):
        if tag == _tag_flair["pers"]:
            return NER._unified_tag["pers"]
        if tag in _tag_flair["place"]:
            return NER._unified_tag["place"]
        if tag == _tag_flair["org"]:
            return NER._unified_tag["org"]

