# coding = utf-8


class NER:
    _unified_tag = {
        "place": "LOC",
        "org": "ORG",
        "pers": "PERS"
    }

    def __init__(self, lang):
        self._lang = lang

    def identify(self, input):
        """

        Parameters
        ----------
        input

        Returns
        -------

        """
        raise NotImplementedError

    def parse_output(self, output):
        """
        Parse the output of the NER
        Parameters
        ----------
        output: obj
            ner output
        Returns
        -------
        2D-array numpy
            First col = Text, Second Col = Tag
        """
        raise NotImplementedError

    def translate_tag(self, tag):
        """
        Translate the NER tag to a unique tag use in this module.
        Parameters
        ----------
        tag :str
            tag

        Returns
        -------
        str
            transformed tag
        """
        raise NotImplementedError