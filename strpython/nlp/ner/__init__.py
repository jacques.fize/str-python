from .spacy import Spacy
from .nltk import NLTK
#from .polyglot import Polyglot
from .stanford_ner import StanfordNER
from .ner import NER