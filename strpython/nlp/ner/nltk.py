# coding = utf-8
import nltk

from .ner import NER
import numpy as np

class NLTK(NER):
    """

    """
    _list_of_tags_available = ["LOCATION", "GPE", "PERSON", "ORGANIZATION"]

    def __init__(self, lang):
        """Constructor for NLTK"""
        NER.__init__(self, lang)
        if lang != "en":
            print("Language may not be supported by NTLK !")

    def identify(self, text):
        tokens = nltk.word_tokenize(text)
        tagged = nltk.pos_tag(tokens)
        ner_tagged = nltk.chunk.ne_chunk(tagged)

        output = []
        for tok_ in ner_tagged:
            if isinstance(tok_, nltk.tree.Tree):
                corresponding_tag_ = self.translate_tag(tok_.label())
                if tok_.label() in NLTK._list_of_tags_available:
                    output.append([" ".join([t[0] for t in tok_]),self.translate_tag(tok_.label())])
        return np.array(output)

    def translate_tag(self, tag):
        if tag == "LOCATION" or tag == "GPE":
            return NER._unified_tag["place"]
        if tag == "ORGANIZATION":
            return NER._unified_tag["org"]
        if tag == "PERSON":
            return NER._unified_tag["pers"]
        return tag
