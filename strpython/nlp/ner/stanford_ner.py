# coding=utf-8

from queue import Queue
from threading import Thread
import json
from pycorenlp import StanfordCoreNLP as RestStanford

from ...config.configuration import config
from .ner import NER
from ..exception.language import LanguageNotAvailable

_stanfordner_available_language = ["fr", "en","es"]

_stanfordner_to_treetagger_lang = {
    "fr": "french",
    "en": "english",
    "es" : "espagnol"
}

_tag_stanford = {"en":{
                    "place": "LOCATION",
                        "org": "ORGANIZATION",
                        "pers": "PERSON"
                    },
                "fr":{
                    "place": "I-LIEU",
                    "org": "I-ORG",
                    "pers": "I-PERS"
                    }

}
nlp_config={"fr" : {
                "tokenize.language" : "fr",
                "pos.model" : "edu/stanford/nlp/models/pos-tagger/french/french.tagger",
                "parse.model" : "edu/stanford/nlp/models/lexparser/frenchFactored.ser.gz",
                "depparse.model" : "edu/stanford/nlp/models/parser/nndep/UD_French.gz",
                "depparse.language" : "french",
                "ner.model":  "/Users/jacquesfize/.services/stanford-corenlp-full-2017-06-09/eunews.fr.crf.gz",
                "ssplit.newlineIsSentenceBreak": "always"
            },
    "en":{}

}


class NERWorker(Thread):

    def __init__(self,ner,queue,lang):
        Thread.__init__(self)
        self.ner=ner
        self.outputs=[]
        self.queue=queue
        self._lang=lang
    def run(self):
        while 1:
            id_,text=self.queue.get()
            self.outputs.append((id_,self.ner.annotate(text, properties={'annotators': 'tokenize,ssplit,pos,ner',
                                                        'outputFormat': 'json',"tokenize.untokenizable": "noneDelete"})))
            self.queue.task_done()
            if self.queue.empty():
                break
class StanfordNER(NER):
    """
    Python wrapper for StanfordNER
    """

    def __init__(self, lang="fr", maxRam="4g"):
        NER.__init__(self, lang)
        self.maxRam = maxRam
        if not self._lang in _stanfordner_available_language:
            print(self._lang)
            raise LanguageNotAvailable(self._lang, self)
        self._ner= RestStanford(config.core_nlp_URL)

        self.identified = None


    def split_text(self,text,maxlen=50000):
        texts=text.split(".")
        phrases_given=[]
        c=0
        current_phrase=""
        for t in texts:
            if c + len(t) <maxlen:
                current_phrase+="."+t
                c+=len(t)
            elif c + len(t) > maxlen:
                phrases_given.append(current_phrase)
                current_phrase, c ="",0
        if not phrases_given:
            phrases_given=[text]
        return phrases_given

    def identify(self, text=None):
        maxlen=10000
        if not text:
            raise TypeError("No value found in `text` parameter.")

        properties = {'annotators': 'tokenize,ssplit,pos,ner', 'outputFormat': 'json',
                      "tokenize.untokenizable": "noneDelete", "pipelineLanguage": self._lang}
        properties.update(nlp_config[self._lang])
        if len(text) < maxlen :
            output_=self._ner.annotate(text,properties=properties)
            if isinstance(output_, str):
                output_ = json.loads(output_, strict=False)
        else:
            texts=self.split_text(text,maxlen)
            output_ = self._ner.annotate(texts[0] ,properties=properties)
            if isinstance(output_, str):
                output_ = json.loads(output_, strict=False)

            queue=Queue()
            queue_texts=[]
            for t in range(1,len(texts)):
                queue.put((t,texts[t]))
            list_worker=[]
            for t in range(4):
                worker=NERWorker(self._ner,queue,self._lang)
                list_worker.append(worker)
                list_worker[-1].daemon=True
                #print(type(list_worker[-1]))
                try:
                    list_worker[-1].start()
                except:
                    print("Worker {0} couldn't be activated !".format(t))
            queue.join()
            queue.queue.clear()
            outputs=["" for i in range(len(texts)-1)]
            for worker in list_worker:
                for id,out in worker.outputs:
                    outputs[id-1]=out
            for o in outputs:
                try:
                    if isinstance(o, str):
                        o = json.loads(o, strict=False)
                    output_["sentences"].extend(o["sentences"])
                except:
                    pass
        return self.parse_output(output_, [])

    def parse_output(self, output, pos_tags):
        tagged_=[]
        _tag_entity = list(_tag_stanford[self._lang].values())

        for sentence in output["sentences"]:
            for w in sentence["tokens"]:
                if w["ner"] in _tag_entity:
                    tagged_.append([w["originalText"],self.translate_tag(w["ner"])])

        return tagged_

    def translate_tag(self,tag):
        if tag == _tag_stanford[self._lang]["pers"]:
            return NER._unified_tag["pers"]
        if tag ==_tag_stanford[self._lang]["place"]:
            return NER._unified_tag["place"]
        if tag ==_tag_stanford[self._lang]["org"]:
            return NER._unified_tag["org"]


# java -mx600m -cp "*:lib\*" edu.stanford.nlp.ie.crf.CRFClassifier -loadClassifier eunews.fr.crf.gz -textFile ../ownCloud/THESE/NoteBookPython/corpus/corpus.txt > test.txt

if __name__ == '__main__':
    import json

    test = StanfordNER()
    print(test.tag("J'aime le président des Etats-Unis, Donald Trump."))

