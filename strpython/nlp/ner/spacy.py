# coding=utf-8

import spacy

import numpy as np
from .ner import NER
from ..exception.language import LanguageNotAvailable

_spacy_available_language = ["fr", "en","es","de"]

_tag_spacy = {
    "place": ["GPE", "LOC"],  # Petite particularité
    "pers": "PERSON",
    "org": "ORG"
}

all_tags=["GPE", "LOC","PERSON","ORG"]

def flatten(lis):
    """
    Given a list, possibly nested to any level, return it flattened.
    """
    new_lis = []
    for item in lis:
        if type(item) == type([]):
            new_lis.extend(flatten(item))
        else:
            new_lis.append(item)
    return new_lis

class Spacy(NER):
    """
    Python wrapper for StanfordNER
    """

    def __init__(self, lang="fr"):
        NER.__init__(self, lang)

        if not self._lang in _spacy_available_language:
            raise LanguageNotAvailable(self._lang, self)

        self._ner = spacy.load(self._lang,disable=['parser', 'tagger'])

    def split_text(self,text,maxlen=50000):
        texts=text.split(".")
        phrases_given=[]
        c=0
        current_phrase=""
        for t in texts:
            if c + len(t)+1 <maxlen:
                current_phrase+="."+t
                c+=len(t)+1
            elif c + len(t) > maxlen:
                phrases_given.append(current_phrase)
                current_phrase, c ="",0
        if not phrases_given:
            phrases_given=[text]
        return phrases_given

    def identify(self, text=None):
        import multiprocessing
        if len(text) > 10000:
            output_=[]
            for t in self._ner.pipe(self.split_text(text,10000),n_threads=multiprocessing.cpu_count(),batch_size=100,as_tuples=False):
                output_.extend([[token.text, self.translate_tag(token.label_)] for token in t.ents])
            return np.array(output_)
        else:
            output_ = [[token.text, self.translate_tag(token.label_)] for token in self._ner(text).ents if token.label_ in all_tags]
            return np.array(output_)

    def translate_tag(self, tag):
        if tag == _tag_spacy["pers"]:
            return NER._unified_tag["pers"]
        if tag in _tag_spacy["place"]:
            return NER._unified_tag["place"]
        if tag == _tag_spacy["org"]:
            return NER._unified_tag["org"]

