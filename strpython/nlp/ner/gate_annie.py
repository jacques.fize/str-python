# coding = utf-8

import requests
from polyglot.text import Text

from .ner import NER


class GateAnnie(NER):
    """"""

    def __init__(self, lang, host="http://localhost:4035"):
        NER.__init__(self, lang)
        self.host = host

    def identify(self, input):
        if not input:
            return []

        response = requests.post(self.host + "/ner", data=input.encode("utf-8")).content
        response = response.decode("utf-8").split("\n")
        response = [r.split("\t") for r in response]

        return self.parse_output(input, response)

    def parse_output(self, input, output):
        # On ne récupère que les "LOC"(ations)
        locations = []
        for i in output:
            if i[1] == "LOC":
                w = i[0].split("-")
                w = [j.split(" ") for j in w]
                if len(w[0]) < 2:
                    w = w[0][0]
                else:
                    w = w[0]
                locations.append([w, i[1:]])

        # print(locations)
        # On récupére le pos_tagging de Polyglot
        old = Text(input).pos_tags
        # print("tagged")
        # print(locations)

        # Puis on extrait notre sortie
        new_ = []
        p = 0
        while p < len(old):
            item = old[p]
            flag = False
            for l in locations:
                possibly = []
                if isinstance(l[0], list) and len(l[0]) > 1:
                    if item[0] == l[0][0]:
                        flag = True
                        possibly.append([item[0], "BEG-LOC"])
                        j = 1
                        while j < len(l[0]):
                            # print(old[j + p], l[0][j])
                            if old[j + p][0] == l[0][j]:
                                if j + 1 == len(l[0]):
                                    possibly.append([old[p + j][0], "END-LOC"])
                                else:
                                    possibly.append([old[p + j][0], "LOC"])
                            else:
                                possibly = []
                                flag = False
                                break
                            j += 1
                        if possibly:
                            new_.extend(possibly)
                            p += j
                            break
                elif item[0] == l[0]:
                    flag = True
                    new_.append([item[0], "LOC"])
                    p += 1
                    break
            if not flag:
                new_.append(list(item))
                p += 1

        return new_
