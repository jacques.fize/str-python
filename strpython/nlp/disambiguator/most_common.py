# coding = utf-8



from ...helpers.geodict_helpers import *
from .disambiguator import Disambiguator
import re, json, os
from ...config.configuration import config

from inflector import Inflector,English,Spanish,French

inflectors= {
    "en":Inflector(English()),
    "fr":Inflector(French()),
    "es":Inflector(Spanish())
}
stop_words = {
    "fr": set(open(os.path.join(config.language_resources_path,"stop_words_fr.txt")).read().split("\n")),
    "en": set(open(os.path.join(config.language_resources_path,"stop_words_en.txt")).read().split("\n"))
}

common_words = {
    "fr": set(json.load(open(os.path.join(config.language_resources_path,"dic_fr.json")))),
    "en": set(open(os.path.join(config.language_resources_path,"english_common_words_filtered.txt")).read().split("\n"))
}


class MostCommonDisambiguator(Disambiguator):

    def __init__(self):
        Disambiguator.__init__(self,one_by_one=True)

    def disambiguate_one_by_one(self, toponyms,lang):
        result={}
        for toponym in toponyms:
            id_,_=self.disambiguate_(toponym,lang)
            if id_:
                result[toponym]=id_
        return result

    def disambiguate_(self, label, lang='fr'):
        if re.match("^\d+$", label):
            return 'O', -1
        if lang in stop_words: #and lang in common_words:
            if label.lower().rstrip("s") in stop_words[lang]:
                return 'O', -1

            if lang in inflectors:
                plural=inflectors[lang].singularize(label)
            else:
                plural = label.rstrip("s") + "s"
            if plural.lower() in stop_words[lang]:
                return 'O', -1

        data=self.get_candidates(label, lang).sort_values(by="score",ascending=False)
        id_, score=None,0
        if len(data)>0:
            entry_selected= data.iloc[0]
            id_,score=entry_selected.id,entry_selected.score
        return id_, score
