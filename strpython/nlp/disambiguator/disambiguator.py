# coding = utf-8

import copy
import string

import numpy as np
import pandas as pd

from ..ner.ner import NER

from ...helpers.geodict_helpers import gazetteer

class Disambiguator(object):

    def __init__(self,one_by_one=False,context_based=False):
        """Constructor for Disambiguator"""
        self.one_by_one= one_by_one
        self.context_based=context_based

    def disambiguate(self,lang,ner_output=None,toponyms=None):
        """
        Run the disambiguation on the NER output
        Parameters
        ----------
        ner_output : 2D numpy array
            NER output
        lang : str
            language

        Returns
        -------
        dict
            {toponym : geodictID}
        """
        if not toponyms:
            if isinstance(ner_output, np.ndarray) and ner_output.shape[1] == 2:
                toponyms = self.parse_ner_output(ner_output)
            elif len(np.asarray(ner_output).shape) != 2:
                return {}

        if self.context_based:
            return self.disambiguate_context_based(toponyms,lang)
        else:
            return self.disambiguate_one_by_one(toponyms,lang)

    def disambiguate_one_by_one(self, toponyms, lang):
        """
        Disambiguation process when toponyms are geocoded one by one.
        Parameters
        ----------
        toponyms :list
            toponyms
        Returns
        -------
        dict
            {toponym : geodictID}
        """
        raise NotImplementedError

    def disambiguate_context_based(self,toponyms,lang):
        """
        Disambiguation process when toponyms are geocoded using each one of them
        Parameters
        ----------
        toponyms :list
            toponyms
        Returns
        -------
        dict
            {toponym : geodictID}
        """
        raise NotImplementedError

    def get_candidates(self,label,lang):

        candidates=[]
        candidates.extend(gazetteer.get_by_label(label,lang,size=3,score=True))
        candidates.extend(gazetteer.get_by_alias(label, lang,size=3,score=True))
        candidates.extend(gazetteer.get_n_label_similar(label,lang, n=3,score=False))
        candidates.extend(gazetteer.get_n_alias_similar(label, lang, n=3,score=False))
        return pd.DataFrame([[
            r.id,
            label,
            r.label[lang],
            r.score if "score" in r else -1,
            r.coord if "coord" in r else {},
            r] for r in candidates],
                     columns="id toponym label score coord raw".split())


    def parse_ner_output(self,ner_output):
        if not isinstance(ner_output,np.ndarray):
            ner_output = np.asarray(ner_output)
        return [toponym[0] if isinstance(toponym[0],str) else " ".join(toponym[0]) for toponym in ner_output[ner_output[:,1] == NER._unified_tag["place"]]]