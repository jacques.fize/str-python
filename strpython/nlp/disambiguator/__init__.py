# coding = utf-8

from .most_common import MostCommonDisambiguator
from .share_prop import ShareProp
from .wikipedia_cooc import WikipediaDisambiguator
from .disambiguator import Disambiguator
