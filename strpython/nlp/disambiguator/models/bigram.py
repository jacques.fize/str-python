# coding = utf-8


class BigramModel:
    def __init__(self,freq={},count={}):
        self.cooc_freq=freq
        self.count_associated=count

    def append(self,uri1,uri2):

        if not uri1 in self.cooc_freq:
            self.cooc_freq[uri1]={}
        if not uri2 in self.cooc_freq[uri1]:
            self.cooc_freq[uri1][uri2]=0
        self.cooc_freq[uri1][uri2]+=1

        self.increment_count(uri2)

    def increment_count(self,uri):
        if not uri in self.count_associated:
            self.count_associated[uri]=0
        self.count_associated[uri]+=1

    def get_coocurence_probability(self, pr1, *args):
        if len(args) < 2:
            print("Only one URI indicated")
            return 0.
        res_=1.
        for u in range(1,len(args)):
            res_*=self.get_bigram_probability(args[0],args[u],pr1)
        return res_


    def get_bigram_probability(self,uri1,uri2,pr1=1):
        nna=0.00000001
        if  uri1 in self.cooc_freq:
            if  uri2 in self.cooc_freq[uri1]:
                return self.cooc_freq[uri1][uri2]
                #return self.count_associated[uri1]/self.cooc_freq[uri1][uri2]
        elif uri2 in self.cooc_freq:
            if uri1 in self.cooc_freq[uri2]:
                return self.cooc_freq[uri2][uri1]
                #return self.count_associated[uri2]/self.cooc_freq[uri2][uri1]

        return nna


