# coding = utf-8
import math

from ...helpers.collision import *
#from ...helpers.geodict_helpers_old import *
from ...helpers.geodict_helpers import *
from .disambiguator import Disambiguator

from ...models.str import get_inclusion_chain


class ShareProp(Disambiguator):

    def __init__(self):
        Disambiguator.__init__(self,context_based=True)

    def fib_formula(self, n):
        """
        Return the fibonacci value.
        Parameters
        ----------
        n : int
            parameter
        Returns
        -------
        int
            fibonnaci value
        """
        if n in [0, 1]: return 0  # Modifying fibonacci behaviour
        golden_ratio = (1 + math.sqrt(5)) / 2
        val = (golden_ratio ** n - (1 - golden_ratio) ** n) / math.sqrt(5)
        return int(round(val))

    def inclusion_log(self, x):
        """
        Return the inclusion log
        Parameters
        ----------
        x : int
            parameter

        Returns
        -------
        int
            inclusion log
        """
        if x==0:
            return 1
        return math.log(x)


    def get_inclusion_score(self, id1, id2):
        """
        Return the inclusion score. Compute the distance between two entities in the hierarchy.
        Parameters
        ----------
        id1 : str
            id of the first spatial entity
        id2 : str
            id of the second spatial entity

        Returns
        -------
        int
            inclusion score
        """
        list1 = get_inclusion_chain(id1, 'P131')
        list2 = get_inclusion_chain(id2, 'P131')
        interP131 = len(list(set(list1).intersection(list2)))
        list1 = get_inclusion_chain(id1, 'P706')
        list2 = get_inclusion_chain(id2, 'P706')
        interP706 = len(list(set(list1).intersection(list2)))
        # return fib_no[interP131]+fib_no[interP706]
        return self.inclusion_log(interP131) + self.inclusion_log(interP706)

    def Adjacency_P47(self, es1, es2):
        """
        Return true, if two spatial entities are found adjacent using the P47 property (share borders) from Wikidata.
        Parameters
        ----------
        id1 : str
            id of the first spatial entity
        id2 : str
            id of the second spatial entity

        Returns
        -------
        bool
            true if adjacent using P47
        """
        # data_1, data_2 = gazetteer.get_by_id(id1)[0], gazetteer.get_by_id(id2)[0]

        if "P47" in es1 and "P47" in es2:
            if es1.id in es2.other.P47 or es2.id in es1.other.P47:
                return True
        return False

    def Adjacency_Hull(self, id1, id2):
        """
        To find if two spatial entities hull "collide"
        Parameters
        ----------
        id1 : str
            id of the first spatial entity
        id2 : str
            id of the second spatial entity

        Returns
        -------
        bool
            if collide
        """
        return collisionTwoSEBoundaries(id1, id2)

    def disambiguateOne(self, spat_candidates, fixed_entities):
        """
        Disambiguate one toponym
        Parameters
        ----------
        spat_candidates
            list of candidates found in the georeferential
        fixed_entities
            entities with no ambiguities

        Returns
        -------

        """
        from ...models.spatial_relation import get_spatial_relations
        all = [cand. id for cand in spat_candidates]
        all.extend([cand. id for cand in fixed_entities])
        relations = get_spatial_relations(all)
        score_dc = {}
        for cand in spat_candidates:
            id_cand = cand.id
            score_dc[id_cand] = 0
            for fixed in fixed_entities:
                id_fixed = fixed.id
                if relations.adjacency[id_cand][id_fixed] :
                    score_dc[id_cand] += 2
                # if self.Adjacency_P47(cand, fixed):
                #     score_dc[id_cand] += 3
                # elif self.Adjacency_Hull(id_cand, id_fixed):
                #     score_dc[id_cand] += 2
                score_dc[id_cand] += self.get_inclusion_score(id_cand, id_fixed)

        m = max(score_dc, key=score_dc.get)
        return m


    def disambiguate_context_based(self,toponyms,lang):
        selected_en = {}
        fixed_entities = {}
        ambiguous_entities = {}
        for topo in toponyms:
            request = self.get_candidates(topo,lang)
            if len(request) > 1:
                ambiguous_entities[topo] = request.raw.values.tolist()
            elif len(request) == 1:
                fixed_entities[topo] = request.iloc[0].raw
        d_amb_results = {}
        for topo in ambiguous_entities:
            d = self.disambiguateOne(ambiguous_entities[topo], fixed_entities.values())
            d_amb_results[topo] = d

        for k, v in fixed_entities.items():
            selected_en[k] = v.id
        for k, v in d_amb_results.items():
            selected_en[k] = v

        return selected_en



