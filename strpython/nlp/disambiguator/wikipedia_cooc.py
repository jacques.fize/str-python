# coding = utf-8
import re
import json
from .disambiguator import Disambiguator
from .models.bigram import BigramModel
import numpy as np
from ...helpers.geodict_helpers import *
from .most_common import stop_words,common_words
import networkx as nx
from .most_common import MostCommonDisambiguator
from ...models.spatial_relation import get_spatial_relations

def read_json(fn):
    return json.load(open(fn,'r'))

class WikipediaDisambiguator(Disambiguator):

    def __init__(self,measure="degree"):
        Disambiguator.__init__(self,context_based=True)
        # Load model
        self.model=BigramModel(read_json(config.wiki_cooc_dis.cooc_freq), read_json(config.wiki_cooc_dis.count))
        self.measure=measure
        self.mostcommon = MostCommonDisambiguator()
        self.geo = False
        self.most = False
        self.normalise = True

    def disambiguate_list(self,toponyms,lang):
        result=self.disambiguate_wiki(toponyms,lang)
        return {k:v for k,v in result.items() if v}

    def disambiguate_context_based(self,toponyms,lang):
        toponyms_filtered=[]
        for toponym in toponyms:
            if re.match("^\d+$", toponym):
                continue
            if lang in stop_words and toponym.lower().rstrip("s") in stop_words[lang]:# or toponym.lower().rstrip("s") in common_words[lang]:
                continue

            plural = toponym.rstrip("s") + "s"
            if lang in stop_words and plural.lower() in stop_words[lang]: #or plural.lower() in common_words[lang]:
                continue
            toponyms_filtered.append(toponym)

        toponyms_filtered=list(set(toponyms_filtered))
        g = nx.Graph()

        possible_candidates = []
        betw_cand={} # indicate which toponym group a candidate belong to #w maybe useless ...
        group_candidate = {} #candidates per toponym

        most_com = set([])

        for toponym in toponyms_filtered:
            candidates = self.get_candidates(toponym, lang)
            if len(candidates)<1:
                continue
            f=False


            if not isinstance(candidates,list):
                candidates = [c.id for ix,c in candidates.iterrows()]
            most_com.add( self.mostcommon.disambiguate(lang,toponyms=[toponym])[toponym])
            group_candidate[toponym] = candidates
            betw_cand[toponym]=candidates
            for n in candidates:
                betw_cand[n]=set(candidates)-set(n)
            possible_candidates.extend(candidates)

        for candidate in possible_candidates:
            g.add_node(candidate, label=gazetteer.get_by_id(candidate)[0].label[lang])

        data_candidate={ca :gazetteer.get_by_id(ca)[0] for ca in possible_candidates}

        for candidate in possible_candidates:
            for candidate2 in possible_candidates:

                d = data_candidate[candidate]
                sc = d.score
                # Compute probability
                prob = self.model.get_coocurence_probability(sc, candidate, candidate2)
                if candidate2 in betw_cand[candidate] or prob < 0.0000001:
                    prob = 0.0
                if (candidate in most_com) or (candidate2 in most_com):
                    prob = 1

                if not candidate == candidate2:
                    # take the lowest co-occurrency between two candidates
                    if g.has_edge(candidate2, candidate) :
                        g.edges[candidate2, candidate]["weight"] += prob
                        # if g.edges[candidate2,candidate]["weight"] < prob:
                        #     continue
                    else:
                        g.add_edge(candidate, candidate2, weight=prob)

        if self.geo or self.normalise:
            if nx.get_edge_attributes(g, "weight"):
                max_weight = np.max([val for _, val in nx.get_edge_attributes(g, "weight").items()])
                for item in list(g.edges(data=True)):
                    src, target, att = item
                    g.edges[src, target]["weight"] = att["weight"] / max_weight

        if self.geo:
            if nx.get_edge_attributes(g,"weight"):
                spatial_relations = get_spatial_relations(possible_candidates)
                for item  in list(g.edges(data=True)):
                    src, target, att = item
                    if spatial_relations["inclusion"][src][target] or spatial_relations["inclusion"][src][target] :
                        g.edges[src, target]["weight"] *= 1.25
                    if spatial_relations["adjacency"][src][target] or spatial_relations["adjacency"][src][target] :
                        g.edges[src, target]["weight"] *= 1.5

        if self.most:
            for item in list(g.edges(data=True)):
                src, target, att = item
                if src in most_com or target in most_com:
                    g.edges[src, target]["weight"] *= 1.25

        selected = {}
        #Take the candidates with the highest degree weighted
        for gr in group_candidate:
            if self.measure == "degree":
                selected[gr] = max(group_candidate[gr], key=lambda x: g.degree(x, weight='weight'))
            elif self.measure == "centrality":
                selected[gr] = max(group_candidate[gr], key=lambda x: nx.closeness_centrality(g, x, distance="weight"))
            else:# degree by default
                selected[gr] = max(group_candidate[gr], key=lambda x: g.degree(x, weight='weight'))
        return selected

