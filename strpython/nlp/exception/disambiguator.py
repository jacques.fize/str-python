# coding = utf-8

from termcolor import colored
class NotADisambiguatorInstance(Exception):
    def __init__(self):
        # Call the base class constructor with the parameters it needs
        super(Exception, self).__init__(colored("Setting disambiguator: Give a Disambiguator or Disambiguator sub-class instance","red"))

