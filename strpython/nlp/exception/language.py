# coding = utf-8
from termcolor import colored

class LanguageNotAvailable(Exception):
    def __init__(self, lang, object):
        # Call the base class constructor with the parameters it needs
        super(Exception, self).__init__("{0} not available for {1}".format(colored(lang,"red"), colored(object.__class__.__name__,"magenta")))
