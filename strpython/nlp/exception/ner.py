#coding = utf-8

from termcolor import colored

class ClassifierNotFound(Exception):
    def __init__(self, file):
        # Call the base class constructor with the parameters it needs
        super(Exception, self).__init__("Classifier at {0} doesn't exists. Check your configuration file !".format(colored(file,"red")))

class BinairyDirectoryNotFound(Exception):
    def __init__(self, dir,object):
        # Call the base class constructor with the parameters it needs
        super(Exception, self).__init__("Binairies for {0} at {1} doesn't exists. Check your configuration file !".format(colored(object.__class__.__name__,"magenta"),colored(dir,"red")))

class NotANERInstance(Exception):
    def __init__(self):
        # Call the base class constructor with the parameters it needs
        super(Exception, self).__init__(colored("Setting Named Entity Recognizer: Give a NER or NER sub-class instance","red"))



