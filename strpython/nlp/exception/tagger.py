# coding = utf-8

from termcolor import colored


class NotATaggerInstance(Exception):
    def __init__(self):
        # Call the base class constructor with the parameters it needs
        super(Exception, self).__init__(
            colored("Setting pos-tagger: Give a Tagger or Tagger sub-class instance", "red"))
