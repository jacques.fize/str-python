from setuptools import setup

setup(
    name='strpython',
    version='1.0b',
    packages=['strpython', 'strpython.nlp', 'strpython.nlp.ner', 'strpython.nlp.exception', 'strpython.nlp.pos_tagger',
              'strpython.nlp.disambiguator', 'strpython.nlp.disambiguator.models', 'strpython.eval', 'strpython.config',
              'strpython.models', 'strpython.models.transformation', 'strpython.helpers'],
    url='',
    license='MIT',
    author='Jacques Fize',
    author_email='jacques.fize@cirad.fr',
    description='',
    package_data={"thematic_str":["resources/terminology/**/*"]}
)
